package SoluApp.SeleniumProofCase;

//import SoluApp.SeleniumProofCase.pages_dashboard.*;
//import SoluApp.SeleniumProofCase.pages_candidateProfile.CandidateProfile_Elements_ViewMode;
//import SoluApp.SeleniumProofCase.pages_candidateProfile.CandidateProfile_Elements_EditMode;
//import SoluApp.SeleniumProofCase.pages_candidates.*;
//import SoluApp.SeleniumProofCase.pages_sitewide.*;
//import SoluApp.SeleniumProofCase.Utilities;


public class SubTests {
	public static int delay = 0;
	
	public static void setDelay(int myDelay){
		delay = myDelay;
	}
	
	/*		Add Candidate		*/
	
//	public static void addCandidate(String firstName, String lastName, String email, String phoneNumber, boolean isValidInput, boolean isUnique) throws Exception {
//		System.out.println("Running addCandidate");
//		Sitewide_Elements_Actions.candidates_headerLink_click();
//		Thread.sleep(delay);
//		Candidates_Elements_Actions.addNewCandidate_button_click();
//		Thread.sleep(delay);
//		Candidates_Actions.createCandidate_instance(firstName, lastName, email, phoneNumber);
//		Thread.sleep(delay);
//		Candidates_Actions.save_createCandidateInstance(isValidInput, isUnique);
//		
//		if (isValidInput && isUnique){verifyCandidate(firstName, lastName, email, phoneNumber);}
//		Thread.sleep(delay);
//	}
//	
//	//OVERLOADED
//	public static void addCandidate(String firstName, String lastName, String email, String phoneNumber, String recruiter, String resourceType,
//			boolean isValidInput, boolean isUnique) throws Exception {
//		System.out.println("Running addCandidateFull");
//		Sitewide_Elements_Actions.candidates_headerLink_click();
//		Thread.sleep(delay);
//		Candidates_Elements_Actions.addNewCandidate_button_click();
//		Thread.sleep(delay);
//		Candidates_Actions.createCandidate_instance(firstName, lastName, email, phoneNumber, recruiter, resourceType);
//		Thread.sleep(delay);
//		Candidates_Actions.save_createCandidateInstance(isValidInput, isUnique);
//		
//		if (isValidInput && isUnique){verifyCandidate(firstName, lastName, email, phoneNumber, recruiter, resourceType);}
//		Thread.sleep(delay);
//	}
//	
//	public static void createCandidate(String firstName, String lastName, String email, String phoneNumber,boolean isValidInput, boolean isUnique) throws Exception {
//		System.out.println("Running createCandidate");
//		Sitewide_Elements_Actions.reset_headerLink_click();
//		Thread.sleep(delay);
//		Dashboard_Elements_Actions.createCandidate_button_click();
//		Thread.sleep(delay);
//		Candidates_Actions.createCandidate_instance(firstName, lastName, email, phoneNumber);
//		Thread.sleep(delay);
//		Candidates_Actions.save_createCandidateInstance(isValidInput, isUnique);
//		
//		if (isValidInput && isUnique){verifyCandidate(firstName, lastName, email, phoneNumber);}
//		Thread.sleep(delay);
//	}
//	
//	//OVERLOADED
//	public static void createCandidate(String firstName, String lastName, String email, String phoneNumber, String recruiter, String resourceType,
//			boolean isValidInput, boolean isUnique) throws Exception {
//		System.out.println("Running createCandidate");
//		Sitewide_Elements_Actions.reset_headerLink_click();
//		Thread.sleep(delay);
//		Dashboard_Elements_Actions.createCandidate_button_click();
//		Thread.sleep(delay);
//		Candidates_Actions.createCandidate_instance(firstName, lastName, email, phoneNumber, recruiter, resourceType);
//		Thread.sleep(delay);
//		Candidates_Actions.save_createCandidateInstance(isValidInput, isUnique);
//		
//		if (isValidInput && isUnique){verifyCandidate(firstName, lastName, email, phoneNumber, recruiter, resourceType);}
//		Thread.sleep(delay);
//	}
//	
//	public static void verifyCandidate_viewMode(String fullName) throws Exception{
//		System.out.println("Running verifyCandidate_viewMode");
//		String firstName;
//		String lastName;
//		firstName = fullName.split(" ")[0];
//		lastName = fullName.split(" ")[1];
//		Utilities.assertEquals(Sitewide_Elements_Verify.isCandidateProfile_text_siteWide_verify(), true, "Not candidate profile page");
//		
//		Utilities.assertEquals(firstName.equals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.firstName_text_xpath()).getText()),
//				true, "First name is incorrect got: " + Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.firstName_text_xpath()).getText()
//				+ " looking for: " + firstName);
//		Utilities.assertEquals(lastName.equals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.lastName_text_xpath()).getText()),
//				true, "Last name is incorrect got: " + Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.lastName_text_xpath()).getText()
//				+ " looking for: " + lastName);
//	}
//	
//	public static void verifyCandidate_editMode(String fullName) throws Exception{
//		System.out.println("Running verifyCandidate_editMode");
//		String firstName;
//		String lastName;
//		firstName = fullName.split(" ")[0];
//		lastName = fullName.split(" ")[1];
//		Utilities.assertEquals(Sitewide_Elements_Verify.isCandidateProfile_text_siteWide_verify(), true, "Not candidate profile page");
//		
//		Utilities.assertEquals(firstName.equals(Utilities.driver.findElement(CandidateProfile_Elements_EditMode.firstName_input_id()).getAttribute("value")),
//				true, "First name is incorrect got: " + Utilities.driver.findElement(CandidateProfile_Elements_EditMode.firstName_input_id()).getAttribute("value")
//				+ " looking for: " + firstName);
//		Utilities.assertEquals(lastName.equals(Utilities.driver.findElement(CandidateProfile_Elements_EditMode.lastName_input_id()).getAttribute("value")),
//				true, "Last name is incorrect got: " + Utilities.driver.findElement(CandidateProfile_Elements_EditMode.lastName_input_id()).getAttribute("value")
//				+ " looking for: " + lastName);
//	}
//	
//	
//	public static void verifyCandidate(String firstName, String lastName, String email, String phoneNumber) throws Exception{
//		System.out.println("Running verifyCandidate");
//		Candidates_Actions.viewCandidate(firstName+" "+lastName);
//		Utilities.assertEquals(Sitewide_Elements_Verify.isCandidateProfile_text_siteWide_verify(), true, "Not candidate profile page");
//		
//		Utilities.assertEquals(firstName.equals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.firstName_text_xpath()).getText()),
//				true, "First name is incorrect got: " + Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.firstName_text_xpath()).getText()
//				+ " looking for: " + firstName);
//		Utilities.assertEquals(lastName.equals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.lastName_text_xpath()).getText()),
//				true, "Last name is incorrect got: " + Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.lastName_text_xpath()).getText()
//				+ " looking for: " + lastName);
//		Utilities.assertEquals(email.equals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.emailAddress_text_xpath()).getText()),
//				true, "Email address is incorrect got: " + Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.emailAddress_text_xpath()).getText()
//				+ " looking for: " + email);
//		Utilities.assertEquals(phoneNumber.equals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.phoneNumber_text_xpath()).getText()),
//				true, "Phone number is incorrect got: " + Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.phoneNumber_text_xpath()).getText()
//				+ " looking for: " + phoneNumber);
//	}
//	
//	//OVERLOADED
//	public static void verifyCandidate(String firstName, String lastName, String email, String phoneNumber, String recruiter, String resourceType) throws Exception{
//		verifyCandidate(firstName, lastName, email, phoneNumber);
//		Utilities.assertEquals(recruiter.equals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.recruiter_text_xpath()).getText()),
//				true, "Recruiter is incorrect");
//		Utilities.assertEquals(resourceType.equals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.resourceType_text_xpath()).getText()),
//				true, "Resource type is incorrect");
//	}
}
