package SoluApp.SeleniumProofCase;

import java.io.File;
import java.io.IOException;
import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.InvalidSelectorException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class Utilities {
	public static WebDriver driver;
	public static int customMaxWaitTime;
	public static int delay;
	private static int screenshotNumber = 1;
	
	public static void setDriver(WebDriver myDriver){
		driver = myDriver;
	}
	
	public static void setWaitTime(int customMaxWaitTime){
		Utilities.customMaxWaitTime = customMaxWaitTime;
	}
	
	public static void setDelay(int myDelay){
		delay = myDelay;
	}
	
	public static String resourceDirectory(){
		//return the directory containing resources such as the profile pic and resume to be uploaded
		if(System.getProperty("os.name").startsWith("Windows")){
			return "C:\\Users\\User\\SMT_Resources\\";
		}
		else{
			return "/home/teambox/SMT_Resources/";
		}
	}
	
	public static String screenshotDirectory(){
		//return the directory containing resources such as the profile pic and resume to be uploaded
		if(System.getProperty("os.name").startsWith("Windows")){
			return "C:\\Users\\User\\SMT_Resources\\Failed_Test_Screenshots\\";
		}
		else{
			return "/home/teambox/SMT_Resources/Failed_Test_Screenshots/";
		}
	}
	
	public static boolean isSMT(){
		//returns true if the current page is the SMT
		return doesElementExist(By.xpath("//div[@class='app_base']"));
	}
	
	public static void acceptAlert() throws Exception{
		while (customMaxWaitTime>0){
			if(isAlertPresent(driver)){
				Alert alert = driver.switchTo().alert();
				alert.accept();
				return;
			}
			Thread.sleep(1000);
			customMaxWaitTime--;
		}
		Assert.fail("No Alert Present");
	}
	
	//OVERLOADED
	public static void acceptAlert(int customMaxWaitTime) throws Exception{
		while (customMaxWaitTime>0){
			if(isAlertPresent(driver)){
				Alert alert = driver.switchTo().alert();
				alert.accept();
				return;
			}
			Thread.sleep(1000);
			customMaxWaitTime--;
		}
		Assert.fail("No Alert Present");
	}
	
	public static boolean doesElementExist(By by) {
		System.out.println("Starting doesElementExist");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		boolean existsBool;
		try{
			existsBool = Utilities.driver.findElements(by).size() != 0;
		}catch(InvalidSelectorException e){
			System.out.println("Rerunning doesElementExist");
			existsBool = doesElementExist(by);
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("Ending doesElementExist");
//		if(existsBool){Utilities.driver.findElement(by).getTagName();}
		return existsBool;
//		return true;
	}
	
	public static boolean isAlertPresent(WebDriver driver){
		try{
			driver.switchTo().alert();
			return true;
		}
		catch(NoAlertPresentException ex){
			return false;
		}
	}
	
	public static void acceptAlert_sitewide(WebDriver driver){
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}
	
	public static String getSelectedValue(WebElement myElement){
		//get selected dropdown value
		String myValue = null;
		myValue = getText(myElement.findElement(By.xpath("//option[@selected]")));
		Utilities.assertNotEquals(myValue, null, "No value selected");
		return myValue;
	}
	
	public static String extractCandidateName(String value){
		String[] elementNameArray = value.toLowerCase().split(" ");
		String elementName = elementNameArray[1];
		for(int i = 2; i < elementNameArray.length; i++){
			//correctly piece together the candidate name
			elementName += (" " + elementNameArray[i]);
		}
		return elementName;
	}
	
	public static void clearElement(By by){
		sendKeys(Utilities.driver.findElement(by), Keys.CONTROL + "a");
		sendKeys(Utilities.driver.findElement(by), Keys.DELETE);
	}
	
	public static void clearElement(WebElement myElement){
		sendKeys(myElement, Keys.CONTROL + "a");
		sendKeys(myElement, Keys.DELETE);
	}
	
	//clear text by hitting backspace the number of times neccessary to delete all the characters
//	public static void clearElement(WebElement myElement) throws Exception{
//		int length = myElement.getAttribute("value").length();
//		System.out.println("element value length: " + length);
//		for(int i = length; i > 0; i--){
//			myElement.sendKeys(Keys.BACK_SPACE);
//		}
//		Thread.sleep(2000);
//	}
	
	/*		Custom Assert functions		*/
	//these are used to add console and potentially log outputs to every assert call
	public static void assertEquals(boolean actual, boolean expected, String output){
		boolean assertPassed = false;
		try{
			Assert.assertEquals(actual, expected, output);
			globalAssertPassed();
			assertPassed = true;
		     //print your message for the case assert pass and/or perform any other event
		}catch (Exception e){
		     //print your message for the case assert fails and/or perform any other event
		     System.out.println("Assert Failed: "+e.getMessage());
		}finally{
			if(!assertPassed){
				globalAssertFailed(output);
			    System.out.println("Actual: " + actual + " Expected: " + expected);
			}
		}
	}
	
	//OVERLOADED
	public static void assertEquals(String actual, String expected, String output){
		boolean assertPassed = false;
		try{
			Assert.assertEquals(actual, expected, output);
			globalAssertPassed();
			assertPassed = true;
		     //print your message for the case assert pass and/or perform any other event
		}catch (Exception e){
		     //print your message for the case assert fails and/or perform any other event
		     System.out.println("Assert Failed: "+e.getMessage());
		}finally{
			if(!assertPassed){
				globalAssertFailed(output);
			    System.out.println("Actual: " + actual + " Expected: " + expected);
			}
		}
	}
	
	//OVERLOADED
	public static void assertEquals(Object actual, Object expected, String output){
		boolean assertPassed = false;
		try{
			Assert.assertEquals(actual, expected, "Error: " + output);
			globalAssertPassed();
			assertPassed = true;
		     //print your message for the case assert pass and/or perform any other event
		}catch (Exception e){
		     //print your message for the case assert fails and/or perform any other event
		     System.out.println("Assert Failed: "+e.getMessage());
		}finally{
			if(!assertPassed){
				globalAssertFailed(output);
			    System.out.println("Actual: " + actual + " Expected: " + expected);
			}
		}
	}
	
	public static void assertNotEquals(boolean actual, boolean expected, String output){
		boolean assertPassed = false;
		try{
			Assert.assertNotEquals(actual, expected, output);
			globalNotAssertPassed();
			assertPassed = true;
			//print your message for the case assert pass and/or perform any other event
		}catch (Exception e){
		     //print your message for the case assert fails and/or perform any other event
		     System.out.println("Assert Failed: "+e.getMessage());
		}finally{
			if(!assertPassed){
				globalNotAssertFailed(output);
			    System.out.println("Actual: " + actual + " Expected Not: " + expected);
			}
		}
	}
	
	//OVERLOADED
	public static void assertNotEquals(String actual, String expected, String output){
		boolean assertPassed = false;
		try{
			Assert.assertNotEquals(actual, expected, output);
			globalNotAssertPassed();
			assertPassed = true;
			//print your message for the case assert pass and/or perform any other event
		}catch (Exception e){
			//print your message for the case assert fails and/or perform any other event
			System.out.println("Assert Failed: "+e.getMessage());
		}finally{
			if(!assertPassed){
				globalNotAssertFailed(output);
			    System.out.println("Actual: " + actual + " Expected Not: " + expected);
			}
		}
	}
	
	//OVERLOADED
	public static void assertNotEquals(Object actual, Object expected, String output){
		boolean assertPassed = false;
		try{
		    Assert.assertNotEquals(actual, expected, "Error: " + output);
		    globalNotAssertPassed();
			assertPassed = true;
		    //print your message for the case assert pass and/or perform any other event
		}catch (Exception e){
		    //print your message for the case assert fails and/or perform any other event
		    System.out.println("Assert Failed: "+e.getMessage());
		}finally{
			if(!assertPassed){
				globalNotAssertFailed(output);
			    System.out.println("Actual: " + actual + " Expected Not: " + expected);
			}
		}
	}
	
	public static void globalAssertPassed(){
		System.out.println("GlobalAssertPassed");
	}
	
	public static void globalAssertFailed(String output){
		takeScreenshot(output);
		System.out.println("GlobalAssertFailed: " + output);
	}
	
	public static void globalNotAssertPassed(){
		System.out.println("GlobalNotAssertPassed");
	}
	
	public static void globalNotAssertFailed(String output){
		takeScreenshot(output);
		System.out.println("GlobalNotAssertFailed: " + output);
	}
	
	public static void takeScreenshot(String output){
		Random rand = new Random();
		String myRandString = "";
		output = output.replace(":", "");
		for(int i = 0; i<10; i++){
			myRandString = myRandString + Integer.toString(rand.nextInt());
		}
		File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		try {
			FileUtils.copyFile(scrFile, new File(screenshotDirectory() + screenshotNumber+"_"+output+"_"+rand.nextInt()+".png"));
			screenshotNumber++;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	////////////////////Element Functions with stale reference fix//////////////////
	
	public static void click(By by) {
        int attempts = 0;
        while(attempts < Constants.attemptCount) {
            try {
                driver.findElement(by).click();
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
	}
	
	public static void click(WebElement myElement) {
        int attempts = 0;
        while(attempts < Constants.attemptCount) {
            try {
                myElement.click();
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
	}
	
	public static String getText(By by) {
        int attempts = 0;
        String myText = "";
        while(attempts < Constants.attemptCount) {
            try {
                myText = driver.findElement(by).getText();
                break;
            } catch(StaleElementReferenceException e) {
            }catch(InvalidSelectorException e){
            }
            attempts++;
        }
        return myText;
	}
	
	public static String getText(WebElement myElement) {
        int attempts = 0;
        String myText = "";
        while(attempts < Constants.attemptCount) {
            try {
                myText = myElement.getText();
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
        return myText;
	}
	
	public static boolean isDisplayed(By by) {
        int attempts = 0;
        boolean result = false;
        while(attempts < Constants.attemptCount) {
            try {
                result = driver.findElement(by).isDisplayed();
                break;
            } catch(StaleElementReferenceException e) {
            }catch(InvalidSelectorException e){
            }
            attempts++;
        }
        return result;
	}
	
	public static boolean isDisplayed(WebElement myElement) {
        int attempts = 0;
        boolean result = false;
        while(attempts < Constants.attemptCount) {
            try {
                result = myElement.isDisplayed();
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
        return result;
	}
	
	public static String getAttribute(By by, String atr) {
        int attempts = 0;
        String myText = "";
        while(attempts < Constants.attemptCount) {
            try {
                myText = driver.findElement(by).getAttribute(atr);
                break;
            } catch(StaleElementReferenceException e) {
            }catch(InvalidSelectorException e){
            }
            attempts++;
        }
        return myText;
	}
	
	public static String getAttribute(WebElement myElement, String atr) {
        int attempts = 0;
        String myText = "";
        while(attempts < Constants.attemptCount) {
            try {
                myText = myElement.getAttribute(atr);
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
        return myText;
	}
	
	public static void sendKeys(By by, String value) {
        int attempts = 0;
        while(attempts < Constants.attemptCount) {
            try {
                driver.findElement(by).sendKeys(value);
                break;
            } catch(StaleElementReferenceException e) {
            }catch(InvalidSelectorException e){
            }
            attempts++;
        }
	}
	
	public static void sendKeys(WebElement myElement, String value) {
        int attempts = 0;
        while(attempts < Constants.attemptCount) {
            try {
                myElement.sendKeys(value);
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
	}
	
	public static void sendKeys(WebElement myElement, Keys value) {
        int attempts = 0;
        while(attempts < Constants.attemptCount) {
            try {
                myElement.sendKeys(value);
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
	}
	
	public static void selectByVisibleText(By by, String value) {
        int attempts = 0;
        while(attempts < Constants.attemptCount) {
            try {
                Select mySelect = new Select(Utilities.driver.findElement(by));
                mySelect.selectByVisibleText(value);
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
	}
	
	public static void selectByVisibleText(WebElement myElement, String value) {
        int attempts = 0;
        while(attempts < Constants.attemptCount) {
            try {
            	Select mySelect = new Select(myElement);
                mySelect.selectByVisibleText(value);
                break;
            } catch(StaleElementReferenceException e) {
            }
            attempts++;
        }
	}
}
