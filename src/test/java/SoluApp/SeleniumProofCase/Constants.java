package SoluApp.SeleniumProofCase;

public class Constants {
	/** A candidate that will always exist */
	public static final String defaultCandidateName = "Stephen Cerbone";
	public static final String defaultUserName = "Stephen Cerbone";
	public static final String defaultAssessmentName = "Self Assessment";
	
	public static final int attemptCount = 5;	//amount of times the webdriver should look for an element before failing the test, hopefully fixes stale element exception
}
