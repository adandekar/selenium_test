package SoluApp.SeleniumProofCase.pages_smSettings;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.Utilities;

public class SMSettings_Actions {
	private static WebElement element = null;
	
	/**
	 * Add a resource type.
	 * @Assumptions None.
	 * @Verification Check if the resource type exists after being added.
	 * @param driver
	 * @param resourceType
	 */
	public static void addResourceType(String resourceType){
		SMSettings_Elements_Actions.newResourceType_textBox_fill(resourceType);
		SMSettings_Elements_Actions.addResource_button_click();
		
		Utilities.assertNotEquals(SMSettings_Actions.findResourceType(resourceType), null, "Resource type not properly added");
	}
	
	/**
	 * Add a candidate level.
	 * @Verification Check if the candidate level exists after being added.
	 * @param driver
	 * @param candidateLevel
	 */
	public static void addCandidateLevel(String candidateLevel){
		SMSettings_Elements_Actions.newCandidateLevel_textBox_fill(candidateLevel);
		SMSettings_Elements_Actions.addCandidateLevel_button_click();
		
		Utilities.assertNotEquals(SMSettings_Actions.findCandidateLevel(candidateLevel), null, "Candidate level not properly added");
	}
	
	/**
	 * Find a resource type.
	 * @param driver
	 * @param resourceType
	 * @return
	 */
	public static WebElement findResourceType(String resourceType){
		Sitewide_Elements_Actions.smSettings_headerLink_click();
		List<WebElement> elementList = Utilities.driver.findElements(SMSettings_Elements.resourceTypeList_list_xpath());
		for(WebElement myElement : elementList){
			if(resourceType.equalsIgnoreCase(myElement.findElement(SMSettings_Elements.resourceTypeName_text_xpath()).getText())){return myElement;}
		}
		return null;
	}
	
	/**
	 * Find a candidate level.
	 * @param driver
	 * @param candidateLevel
	 * @return
	 */
	public static WebElement findCandidateLevel(String candidateLevel){
		Sitewide_Elements_Actions.smSettings_headerLink_click();
		List<WebElement> elementList = Utilities.driver.findElements(SMSettings_Elements.candidateLevelList_list_xpath());
		for(WebElement myElement : elementList){
			if(candidateLevel.equalsIgnoreCase(myElement.findElement(SMSettings_Elements.candidateLevelName_text_xpath()).getText())){return myElement;}
		}
		return null;
	}
	
	/**
	 * Remove a resource type.
	 * @Verification check if resource type exists
	 * @param driver
	 * @param resourceType
	 */
	public static void removeResourceType(String resourceType){
		element = SMSettings_Actions.findResourceType(resourceType).findElement(Sitewide_Elements.removeListElement_dynamicList_linkText());
		element.click();
		
		Utilities.assertEquals(SMSettings_Actions.findResourceType(resourceType), null, "Resource not removed");
	}
	
	/**
	 * Remove a candidate level.
	 * @Verification check if candidate level exists
	 * @param driver
	 * @param candidateLevel
	 */
	public static void removeCandidateLevel(String candidateLevel){
		element = SMSettings_Actions.findCandidateLevel(candidateLevel).findElement(Sitewide_Elements.removeListElement_dynamicList_linkText());
		element.click();
		
		Utilities.assertEquals(SMSettings_Actions.findCandidateLevel(candidateLevel), null, "Candidate level not found");
	}
	
	/**
	 * Add a profile status
	 * @param parentStatus
	 * @param profileStatusName
	 * @param profileStatusColor
	 */
	public static void addProfileStatus(String parentStatus, String profileStatusName, String profileStatusColor){
		SMSettings_Elements_Actions.selectParentStatus_dropDown_select(parentStatus);
		
		SMSettings_Elements_Actions.profileStatusName_textBox_fill(profileStatusName);
		
		SMSettings_Elements_Actions.selectProfileStatusColor_dropDown_select(profileStatusColor);
		
		SMSettings_Elements_Actions.addProfileStatus_button_click();
		
		Utilities.assertNotEquals(findProfileStatus(parentStatus, profileStatusName, profileStatusColor), null, "Profile status not properly added.");
	}
	
	/**
	 * Find the specified profile status, under the correct parent status and with the correct color
	 * @param parentStatus
	 * @param profileStatusName
	 * @param profileStatusColor
	 * @return
	 */
	public static WebElement findProfileStatus(String parentStatus, String profileStatusName, String profileStatusColor){
		List<WebElement> profileStatusList = Utilities.driver.findElements(SMSettings_Elements.profileStatusList_list_id());
		System.out.println("Parent: " + parentStatus + " name: " + profileStatusName + " color: " + profileStatusColor);
		if (profileStatusList.size()>0){
			for(WebElement profileStatus : profileStatusList){
//				System.out.println("name: " + profileStatus.findElement(By.xpath(".//div[@class='detail_1']//input[contains(@name,'][0]')]")).getAttribute("value"));
//				System.out.println("parent: " + profileStatus.findElement(By.xpath(".//div[@class='detail_1']//input[contains(@name,'][1]')]")).getAttribute("value"));
//				System.out.println("color: " + profileStatus.findElement(By.xpath(".//div[@class='detail_1']//input[contains(@name,'][2]')]")).getAttribute("value"));
				if(!profileStatusName.equalsIgnoreCase(profileStatus.findElement(By.xpath(".//div[@class='detail_1']//input[contains(@name,'][0]')]")).getAttribute("value"))
						&& !profileStatusName.equals("")){continue;}
				if(!profileStatus.findElement(By.xpath(".//div[@class='detail_1']//input[contains(@name,'][1]')]")).getAttribute("value").toLowerCase().contains(parentStatus.toLowerCase())
						&& !parentStatus.equals("")){continue;}
				if(!profileStatusColor.equalsIgnoreCase(profileStatus.findElement(By.xpath(".//div[@class='detail_1']//input[contains(@name,'][2]')]")).getAttribute("value"))
						&& !profileStatusColor.equals("") && !profileStatusColor.equals("transparent")){continue;}
				return profileStatus;
			}
		}
		return null;
	}
	
	public static void removeProfileStatus(String parentStatus, String profileStatusName, String profileStatusColor){
		element = findProfileStatus(parentStatus, profileStatusName, profileStatusColor);
		Utilities.assertNotEquals(element, null, "Didn't find profile status to remove.");
		element.findElement(Sitewide_Elements.removeListElement_dynamicList_linkText()).click();
		Utilities.assertEquals(findProfileStatus(parentStatus, profileStatusName, profileStatusColor), null, "Profile status not properly removed.");
	}
	
}
