package SoluApp.SeleniumProofCase.pages_smSettings;

import org.openqa.selenium.By;

public class SMSettings_Elements {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	public static By newResourceType_textBox_id(){
		return By.id("new_resource_type");
	}
	
	public static By addResource_button_id(){
		return By.id("resources_add_btn");
	}
	
	public static By newCandidateLevel_textBox_id(){
		return By.id("new_candidate_level");
	}
	
	public static By addCandidateLevel_button_id(){
		return By.id("candidate_levels_add_btn");
	}
	
	public static By profileStatusList_list_id(){
		return By.xpath("//div[@id='details_candidate_profile_statuses']//div[contains(@id,'candidate_profile_status_data_')]");
	}
	
	public static By parentStatus_dropDown_id(){
		return By.id("new_candidate_profile_status_parent");
	}
	
	public static By profileStatusName_textBox_id(){
		return By.id("new_candidate_profile_status");
	}
	
	public static By profileStatusColor_dropDown_id(){
		return By.id("new_candidate_profile_status_color");
	}
	
	public static By profileStatusAdd_button_id(){
		return By.id("candidate_status_add_btn");
	}
	
	/////////List elements//////////
	
	public static By resourceTypeList_list_xpath(){
		return By.xpath("//div[@id='details_resource_type']//div[contains(@id,'resource_data_')]");
	}
	
	public static By resourceTypeName_text_xpath(){
		return By.xpath(".//div[@class='detail_1']");
	}
	
	public static By candidateLevelList_list_xpath(){
		return By.xpath("//div[@id='details_candidate_level']//div[contains(@id,'candidate_data_')]");
	}
	
	public static By candidateLevelName_text_xpath(){
		return By.xpath(".//div[@class='detail_1']");
	}
}
