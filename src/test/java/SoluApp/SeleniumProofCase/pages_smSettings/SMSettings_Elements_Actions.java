package SoluApp.SeleniumProofCase.pages_smSettings;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.Utilities;

public class SMSettings_Elements_Actions {
	private static WebElement element = null;
	
	public static void newResourceType_textBox_fill(String value){
		Utilities.assertEquals(Sitewide_Elements_Verify.isSMSettings_text_siteWide_verify(), true, "Not SMSettings page");
		element = Utilities.driver.findElement(SMSettings_Elements.newResourceType_textBox_id());
		element.sendKeys(value);
	}
	
	public static void addResource_button_click(){
		element = Utilities.driver.findElement(SMSettings_Elements.addResource_button_id());
		element.click();
	}
	
	public static void newCandidateLevel_textBox_fill(String value){
		Utilities.assertEquals(Sitewide_Elements_Verify.isSMSettings_text_siteWide_verify(), true, "Not SMSettings page");
		element = Utilities.driver.findElement(SMSettings_Elements.newCandidateLevel_textBox_id());
		element.sendKeys(value);
	}
	
	public static void addCandidateLevel_button_click(){
		element = Utilities.driver.findElement(SMSettings_Elements.addCandidateLevel_button_id());
		element.click();
	}
	
	public static void selectParentStatus_dropDown_select(String value){
		if(!value.equals("")){
			Select parentStatusDropDown = new Select(Utilities.driver.findElement(SMSettings_Elements.parentStatus_dropDown_id()));
			parentStatusDropDown.selectByVisibleText(value);
		}
	}
	
	public static void profileStatusName_textBox_fill(String value){
		element = Utilities.driver.findElement(SMSettings_Elements.profileStatusName_textBox_id());
		element.sendKeys(value);
	}
	
	public static void selectProfileStatusColor_dropDown_select(String value){
		if(!value.equals("")){
			Select colorDropDown = new Select(Utilities.driver.findElement(SMSettings_Elements.profileStatusColor_dropDown_id()));
			colorDropDown.selectByVisibleText(value);
		}
	}
	
	public static void addProfileStatus_button_click(){
		element = Utilities.driver.findElement(SMSettings_Elements.profileStatusAdd_button_id());
		element.click();
	}
	
	//TODO: add profile status button
}
