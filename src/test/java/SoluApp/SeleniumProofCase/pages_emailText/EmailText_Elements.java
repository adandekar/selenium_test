package SoluApp.SeleniumProofCase.pages_emailText;

import org.openqa.selenium.By;

public class EmailText_Elements {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	////////////////Email Data Input/////////////////
	
	public static By fromName_textBox_id(){
		return By.id("from_name");
	}
	
	public static By fromEmail_textBox_id(){
		return By.id("from_email");
	}
	
	public static By subject_textBox_id(){
		return By.id("subject");
	}
	
	public static By emailBody_textBox_id(){
		return By.id("email_default_text");
	}
	
	public static By adminNotificationEmails_textBox_id(){
		return By.id("admin_notification_emails");
	}
	
	public static By adminNotificationSubject_textBox_id(){
		return By.id("admin_notification_subject");
	}
	
	public static By adminNotificationMessage_textBox_id(){
		return By.id("admin_notification_message");
	}
	
	public static By save_button_xpath(){
		return By.xpath("//form[@id='email_text_form']//input[@class='button_style' and @value='SAVE']");
	}
	
}
