package SoluApp.SeleniumProofCase.pages_emailText;

import org.openqa.selenium.WebElement;

import SoluApp.SeleniumProofCase.Utilities;

public class EmailText_Elements_Actions {
	private static WebElement element = null;
	
	public static void fromName_textBox_fill(String value){
		element = Utilities.driver.findElement(EmailText_Elements.fromName_textBox_id());
		element.sendKeys(value);
	}
	
	public static void fromEmail_textBox_fill(String value){
		element = Utilities.driver.findElement(EmailText_Elements.fromEmail_textBox_id());
		element.sendKeys(value);
	}
	
	public static void subject_textBox_fill(String value){
		element = Utilities.driver.findElement(EmailText_Elements.subject_textBox_id());
		element.sendKeys(value);
	}
	
	public static void emailBody_textBox_fill(String value){
		element = Utilities.driver.findElement(EmailText_Elements.emailBody_textBox_id());
		element.sendKeys(value);
	}
	
	public static void adminNotificationEmails_textBox_fill(String value){
		element = Utilities.driver.findElement(EmailText_Elements.adminNotificationEmails_textBox_id());
		element.sendKeys(value);
	}
	
	public static void adminNotificationSubject_textBox_fill(String value){
		element = Utilities.driver.findElement(EmailText_Elements.adminNotificationSubject_textBox_id());
		element.sendKeys(value);
	}
	
	public static void adminNotificationMessage_textBox_fill(String value){
		element = Utilities.driver.findElement(EmailText_Elements.adminNotificationMessage_textBox_id());
		element.sendKeys(value);
	}
	
	public static void save_button_click(){
		element = Utilities.driver.findElement(EmailText_Elements.save_button_xpath());
		element.click();
	}
}
