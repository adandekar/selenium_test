package SoluApp.SeleniumProofCase.pages_emailText;

public class EmailText_Actions {
	/**
	 * Add assessment in the SG Settings page
	 * @param assessmentName
	 * @param sgID
	 * @param quizActionID
	 * @param totalQue
	 * @param type
	 */
	public static void editEmailText(String fromName, String fromEmail, String subject, String emailBody, String adminNotificationEmails, String adminNotificationSubject,
			String adminNotificationMessage) throws Exception{
		EmailText_Elements_Actions.fromName_textBox_fill(fromName);
		EmailText_Elements_Actions.fromEmail_textBox_fill(fromEmail);
		EmailText_Elements_Actions.subject_textBox_fill(subject);
		EmailText_Elements_Actions.emailBody_textBox_fill(emailBody);
		EmailText_Elements_Actions.adminNotificationEmails_textBox_fill(adminNotificationEmails);
		EmailText_Elements_Actions.adminNotificationSubject_textBox_fill(adminNotificationSubject);
		EmailText_Elements_Actions.adminNotificationMessage_textBox_fill(adminNotificationMessage);
		EmailText_Elements_Actions.save_button_click();
//		Thread.sleep(5000);
	}
}
