package SoluApp.SeleniumProofCase.pages_emailText;

import java.lang.reflect.Method;

import org.openqa.selenium.By;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_sgSettings.SGSettings_Elements_Actions;
import SoluApp.SeleniumProofCase.pages_sitewide.Sitewide_Elements_Actions;
import SoluApp.SeleniumProofCase.Constants;

public class EmailText_Elements_Verify {
	public static void verify() throws Exception{
		Sitewide_Elements_Actions.sgSettings_headerLink_click();
		SGSettings_Elements_Actions.emailText_link_click(Constants.defaultAssessmentName);
		Method[] methods = EmailText_Elements.class.getMethods();
		for (Method m : methods){
			if(m.getName().toLowerCase().contains("xpath") || m.getName().toLowerCase().contains("id")
					|| m.getName().toLowerCase().contains("linktext") || m.getName().toLowerCase().contains("classname")){
				System.out.println("Checking element: " + m.getName());
				Utilities.assertEquals(Utilities.doesElementExist((By)m.invoke(EmailText_Elements.class)), true,
						"The element - " + m.getName() + " does not exist at - " + m.invoke(EmailText_Elements.class));
			}
		}
	}
}
