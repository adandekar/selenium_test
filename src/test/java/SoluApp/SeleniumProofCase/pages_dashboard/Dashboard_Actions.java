package SoluApp.SeleniumProofCase.pages_dashboard;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_candidateProfile.CandidateProfile_Actions;
import SoluApp.SeleniumProofCase.pages_sitewide.Sitewide_Actions;
import SoluApp.SeleniumProofCase.pages_sitewide.Sitewide_Elements_Actions;

public class Dashboard_Actions {
	public static void addCandidate(String firstName, String lastName, String email, String phoneNumber, String recruiter, String resourceType,
			boolean isValidInput, boolean isUnique) throws Exception {
		System.out.println("Running createCandidate");
		Sitewide_Elements_Actions.reset_headerLink_click();
		Thread.sleep(Utilities.delay);
		Dashboard_Elements_Actions.createCandidate_button_click();
		Thread.sleep(Utilities.delay);
		Sitewide_Actions.fillCandidateInformation(firstName, lastName, email, phoneNumber, recruiter, resourceType);
		Thread.sleep(Utilities.delay);
		Sitewide_Actions.save_createCandidateInstance(isValidInput, isUnique);
		
		if (isValidInput && isUnique){CandidateProfile_Actions.verifyCandidate(firstName, lastName, email, phoneNumber, recruiter, resourceType);}
		Thread.sleep(Utilities.delay);
	}
}
