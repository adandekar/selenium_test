package SoluApp.SeleniumProofCase.pages_dashboard;

import org.openqa.selenium.WebElement;

import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.Utilities;

public class Dashboard_Elements_Actions {
	private static WebElement element = null;
	
	public static void createCandidate_button_click(){
		//System.out.println("Starting create candidate helper function!");
		element = Utilities.driver.findElement(Dashboard_Elements.createCandidate_button_id());
		//System.out.println("found element?");
		element.click();
		
		//Assert
		element = Utilities.driver.findElement(Dashboard_Elements.candidatePopup_popup_id());
//		Utilities.assertEquals(element.getAttribute("style"), "display: block;", "Popup did not open properly");
		Utilities.assertEquals(element.isDisplayed(), true, "Popup failed to open");
	}
	
	////////////////////Side Links///////////////////////
	public static void dashboard_button_sideBar_click(){
		element = Utilities.driver.findElement(Dashboard_Elements.dashboard_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isDashboard_text_siteWide_verify(), true, "Dashboard link failed");
	}
	
	public static void candidates_button_sideBar_click(){
		element = Utilities.driver.findElement(Dashboard_Elements.candidates_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isCandidates_text_siteWide_verify(), true, "Candidates link failed");
	}
	
	public static void assignTest_button_sideBar_click(){
		element = Utilities.driver.findElement(Dashboard_Elements.assignTest_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isAssignTest_text_siteWide_verify(), true, "Assign test link failed");
	}
	
	public static void apiResults_button_sideBar_click(){
		element = Utilities.driver.findElement(Dashboard_Elements.apiResults_button_id());
		element.click();
	}
	
	public static void sgSettings_button_sideBar_click(){
		element = Utilities.driver.findElement(Dashboard_Elements.sgSettings_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isSGSettings_text_siteWide_verify(), true, "SGSettings link failed");
	}
	
	public static void smSettings_button_sideBar_click(){
		element = Utilities.driver.findElement(Dashboard_Elements.smSettings_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isSMSettings_text_siteWide_verify(), true, "SMSettings link failed");
	}
	
	public static void userManagement_button_sideBar_click(){
		element = Utilities.driver.findElement(Dashboard_Elements.userManagement_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isUserManagement_text_siteWide_verify(), true, "User management link failed");
	}
}
