package SoluApp.SeleniumProofCase.pages_dashboard;

import org.openqa.selenium.By;

public class Dashboard_Elements {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	public static By createCandidate_button_id(){
		return By.id("admin_link_candidate");
	}
	
	public static By candidatePopup_popup_id(){
		return By.id("candidate_popup");
	}
	
	public static By dashboard_button_id(){
		return By.id("admin_link_dashboard");
	}
	
	public static By candidates_button_id(){
		return By.id("admin_link_candidates");
	}
	
	public static By assignTest_button_id(){
		return By.id("admin_link_assign");
	}
	
	public static By apiResults_button_id(){
		return By.id("admin_link_api_results");
	}
	
	public static By sgSettings_button_id(){
		return By.id("admin_link_sg_settings");
	}
	
	public static By smSettings_button_id(){
		return By.id("admin_link_sm_settings");
	}
	
	public static By userManagement_button_id(){
		return By.id("admin_link_users");
	}
}
