package SoluApp.SeleniumProofCase.pages_candidateProfile;

import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.Utilities;

public class CandidateProfile_Elements_Actions {
	private static WebElement element = null;
	
	public static void view_button_click(){
		Utilities.assertEquals(Sitewide_Elements_Verify.isEditMode_buttonId_candidateProfile_verify(), true, "Not in edit mode");
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.viewMode_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isViewMode_buttonId_candidateProfile_verify(), true, "View mode button failed");
	}
	
	public static void edit_button_click(){
		Utilities.assertEquals(Sitewide_Elements_Verify.isViewMode_buttonId_candidateProfile_verify(), true, "Not in view mode");
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.editMode_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isEditMode_buttonId_candidateProfile_verify(), true, "Edit mode button failed");
	}
	
	public static void downloadResume_button_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.downloadResume_dynamicButton_id());
		element.click();
	}
	
	public static void save_button_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.save_button_id());
		element.click();
	}
	
	public static void sendSelected_button_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.sendSelected_button_id());
		element.click();
	}
	
	public static void sendThankYouEmail_button_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.sendThankYouEmail_button_id());
		element.click();
		
		//Assert
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.thankYouPopup_popup_id());
		Utilities.assertEquals(element.isDisplayed(), true, "Popup failed to open");
	}
	
	public static void send_button_thankYouEmailPopup_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.thankYouPopupSend_button_id());
		element.click();
	}
	
	///////////////////Get Candidate Information///////////////////////
	
	public static String getFirstName_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.firstName_input_id());
		return element.getAttribute("value");
	}
	
	public static String getLastName_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.lastName_input_id());
		return element.getAttribute("value");
	}
	
	public static String getEmailAddress_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.emailAddress_input_xpath());
		return element.getAttribute("value");
	}
	
	public static String getUserName_text_get(){
		String emailAddress = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.emailAddress_text_xpath()).getText();
		return emailAddress.split("@")[0];
	}
	
	public static String getPhoneNumber_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.phoneNumber_input_id());
		return element.getAttribute("value");
	}
	
	public static String getAddress_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.address_input_id());
		return element.getAttribute("value");
	}
	
	public static String getCity_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.city_input_id());
		return element.getAttribute("value");
	}
	
	public static String getState_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.state_input_id());
		return element.getAttribute("value");
	}
	
	public static String getZipCode_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.zipCode_input_id());
		return element.getAttribute("value");
	}
	public static String getResourceType_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.resourceType_input_id());
		return element.getAttribute("value");
	}
	public static String getLevel_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.level_input_id());
		return element.getAttribute("value");
	}
	public static String getLocation_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.location_input_id());
		return element.getAttribute("value");
	}
	public static String getRecruiter_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.recruiter_input_id());
		return element.getAttribute("value");
	}
	public static String getTags_textBox_get(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.tags_list_id());
		return element.getAttribute("value");
	}
	
	//////////////////Set Candidate Information/////////////////////
	public static void setFirstName_textBox_set(String value) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.firstName_input_id());
		Utilities.clearElement(element);
		element.sendKeys(value);
	}
	
	public static void setLastName_textBox_set(String value) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.lastName_input_id());
		Utilities.clearElement(element);
		element.sendKeys(value);
	}
	
	public static void setEmailAddress_textBox_set(String value) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.emailAddress_input_xpath());
		Utilities.clearElement(element);
		element.sendKeys(value);
	}
	
	public static void setPhoneNumber_textBox_set(String value) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.phoneNumber_input_id());
		Utilities.clearElement(element);
		element.sendKeys(value);
	}
	
	public static void setAddress_textBox_set(String value) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.address_input_id());
		Utilities.clearElement(element);
		element.sendKeys(value);
	}
	
	public static void setCity_textBox_set(String value) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.city_input_id());
		Utilities.clearElement(element);
		element.sendKeys(value);
	}
	
	public static void setState_textBox_set(String value) throws Exception{
		Select dropDown = new Select(Utilities.driver.findElement(CandidateProfile_Elements_EditMode.state_input_id()));
		dropDown.selectByVisibleText(value);
	}
	
	public static void setZipCode_textBox_set(String value) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.zipCode_input_id());
		Utilities.clearElement(element);
		element.sendKeys(value);
	}
	
	public static void setResourceType_textBox_set(String value) throws Exception{
		Select dropDown = new Select(Utilities.driver.findElement(CandidateProfile_Elements_EditMode.resourceType_input_id()));
		dropDown.selectByVisibleText(value);
	}
	
	public static void setLevel_textBox_set(String value) throws Exception{
		Select dropDown = new Select(Utilities.driver.findElement(CandidateProfile_Elements_EditMode.level_input_id()));
		dropDown.selectByVisibleText(value);
	}
	
	public static void setLocation_textBox_set(String value) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.location_input_id());
		Utilities.clearElement(element);
		element.sendKeys(value);
	}
	
	public static void setRecruiter_textBox_set(String value) throws Exception{
		Select dropDown = new Select(Utilities.driver.findElement(CandidateProfile_Elements_EditMode.recruiter_input_id()));
		dropDown.selectByVisibleText(value);
	}
	
	public static void addTag_input_set(String value) throws Exception{
		
	}
	
	public static void setTags_textBox_set(String[] tags) throws Exception{
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.tags_list_id());
		element.click();
		if (Utilities.doesElementExist(By.xpath("//ul[@id='taggle_list']//li[@class='taggle']"))){
			for (@SuppressWarnings("unused") WebElement myElement : Utilities.driver.findElements(By.xpath("//ul[@id='taggle_list']//li[@class='taggle']"))){
				element.sendKeys(Keys.BACK_SPACE);
				element.sendKeys(Keys.BACK_SPACE);
			}
		}
		
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.enterTag_input_xpath());
		
		for (String tag : tags){
			element.sendKeys(tag);
			element.sendKeys(Keys.RETURN);
		}
	}
	
	///////////////Get Assessment Data///////////////////
	public static String[] getAssessmentBody(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.assessmentBody_textArea_id());
		String myEmailBody = element.getText().replaceAll("\\[(.*?)\\]", "%replace%");
		return myEmailBody.split("%replace%");
	}
	
	public static String[] getAssessmentAdminBody(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.assessmentAdminEmailBody_textArea_id());
		String myEmailBody = element.getText().replaceAll("\\[(.*?)\\]", "%replace%");
		return myEmailBody.split("%replace%");
	}
	
	public static String getAssessmentSubject(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.assessmentSubject_textBox_id());
		return element.getText().replaceAll("\\[(.*?)\\]", "");
	}
	
	public static String getAssessmentFromEmail(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.assessmentFromEmail_textBox_id());
		return element.getText();
	}
	
	////////////////Profile Pic and Resume////////////////
	public static void uploadNewPhoto_link_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.uploadNewPhoto_link_id());
		element.click();
	}
	
	public static void removePhoto_link_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.removePhoto_link_id());
		element.click();
	}
	
	public static void uploadResume_button_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.uploadResume_button_id());
		element.click();
	}
	
	public static void uploadPhotoSendDirectory_link_fill(String directory){
		System.out.println("Directory: " + directory);
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.uploadProfilePic_input_id());
		element.sendKeys(directory);
	}
	
	public static void uploadResumeDirectory_link_fill(String directory){
		System.out.println("Directory: " + directory);
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.uploadResume_input_id());
		element.sendKeys(directory);
	}
	
	public static boolean checkPhoto_image_check(int pictureSize) throws Exception{
		System.out.println("Attribute: " + Utilities.getAttribute(CandidateProfile_Elements_ViewMode.profilePicture_image_id(), "src") +
				" looking for: images/no-image.png");
//		System.out.println("Image height: " + Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.profilePicture_image_id()).getSize().getHeight());
//		System.out.println("Image width: " + Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.profilePicture_image_id()).getSize().getWidth());
		URLConnection urlConnection = new URL(Utilities.driver.findElement(CandidateProfile_Elements_EditMode.profilePicture_image_id()).getAttribute("src")).openConnection();
		int size = urlConnection.getContentLength();
		System.out.println("Target Image size: " + pictureSize + " Actual size: " + size);
		return (!Utilities.getAttribute(CandidateProfile_Elements_ViewMode.profilePicture_image_id(), "src").contains("images/no-image.png")
				&& Utilities.getAttribute(CandidateProfile_Elements_ViewMode.profilePicture_image_id(), "src").contains(
						CandidateProfile_Elements_Actions.getEmailAddress_textBox_get()) &&
						size==pictureSize);
	}
	
	////////////////Social Media///////////////////////
	
	public static List<WebElement> getSocialMedia_list_get(){
		if(Sitewide_Elements_Verify.isCandidateProfile_text_siteWide_verify() &&
				Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.socialMedia_dynamicList_xpath())){
			return Utilities.driver.findElements(CandidateProfile_Elements_ViewMode.socialMedia_dynamicList_xpath());
		}
		return null;
	}
	
	public static void heading_textBox_fill(String value){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.socialMediaHeading_textBox_id());
		element.sendKeys(value);
	}
	
	public static void link_textBox_fill(String value){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.socialMediaLink_textBox_id());
		element.sendKeys(value);
	}
	
	public static void add_button_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_EditMode.socialMediaAdd_button_id());
		element.click();
	}
	
	//////////////Comments//////////////////////////
	
	public static List<WebElement> getComments_list_get(){
		if(Sitewide_Elements_Verify.isViewMode_buttonId_candidateProfile_verify() &&
				Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.commentsList_dynamicList_xpath())){
			return Utilities.driver.findElements(CandidateProfile_Elements_ViewMode.commentsList_dynamicList_xpath());
		}
		return null;
	}
	
	public static void comment_textArea_fill(String value){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.commentsText_textArea_id());
		element.sendKeys(value);
	}
	
	public static void addComment_button_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.addComment_button_id());
		element.click();
	}
	
	//////////////Assign New Test Popup////////////////
	
	public static void proceed_button_assignNewTest_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.proceed_button_id());
		element.click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isAssigned_text_siteWide_verify(), true, "Proceed button failed");
	}
	
	public static void cancel_button_assignNewTest_click(){
		element = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.cancel_button_id());
		element.click();
	}
}
