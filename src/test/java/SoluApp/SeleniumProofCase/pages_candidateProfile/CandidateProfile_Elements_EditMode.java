package SoluApp.SeleniumProofCase.pages_candidateProfile;

import org.openqa.selenium.By;

public class CandidateProfile_Elements_EditMode {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	public static By heading_text_xpath(){
		return By.xpath("//h1[@class='profile_heading']");
	}
	
	public static By viewMode_button_id(){
		return By.id("view_mode_btn");
	}
	
	public static By save_button_id(){
		return By.id("save_btn");
	}
	
	//////////////Candidate Profile Information/////////////////////
	
	public static By firstName_input_id(){
		return By.id("candidate_first_name");
	}
	
	public static By lastName_input_id(){
		return By.id("candidate_last_name");
	}
	
	public static By emailAddress_input_xpath(){
		return By.xpath("//div[@class='candidate_profile_details']//input[@id='candidate_email']");
	}
	
	public static By phoneNumber_input_id(){
		return By.id("candidate_phone");
	}
	
	public static By address_input_id(){
		return By.id("candidate_address");
	}
	
	public static By city_input_id(){
		return By.id("candidate_city");
	}
	
	public static By state_input_id(){
		return By.id("candidate_state");
	}
	
	public static By zipCode_input_id(){
		return By.id("candidate_zip");
	}
	
	public static By resourceType_input_id(){
		return By.id("candidate_resource_type");
	}
	
	public static By level_input_id(){
		return By.id("candidate_level");
	}
	
	public static By location_input_id(){
		return By.id("candidate_location");
	}
	
	public static By recruiter_input_id(){
		return By.id("candidate_recruiter");
	}
	
	public static By tags_list_id(){
		return By.id("taggle_list");
	}
	
	public static By enterTag_input_xpath(){
		return By.xpath("//ul[@id='taggle_list']//input[@id='taggle_input']");
	}
	
	
	///////////////Profile Pic and Resume///////////////
	
	public static By profilePicture_image_id(){
		return By.id("photo_display");
	}
	
	public static By uploadNewPhoto_link_id(){
		return By.id("upload_photo");
	}
	
	public static By removePhoto_link_id(){
		return By.id("remove_photo");
	}
	
	public static By uploadResume_button_id(){
		return By.id("upload_resume_btn");
	}
	
	public static By uploadProfilePic_input_id(){
		return By.id("fileToUpload");
	}
	
	public static By uploadResume_input_id(){
		return By.id("fileToUploadResume");
	}
	
	///////////////Assessments/////////////////////
	public static By assessmentCheckbox_checkBox_id(){
		return By.id("assessment_checkbox");
	}
	
	public static By sendSelected_button_id(){
		return By.id("send_selected_btn");
	}
	
	public static By sendThankYouEmail_button_id(){
		return By.id("thankyou_email_btn");
	}
	
	public static By thankYouPopup_popup_id(){
		return By.id("thankyou_popup");
	}
	
	public static By thankYouPopupSend_button_id(){
		return By.id("thankyou_send_btn");
	}
	
	////////////////Social Media/////////////////
	
	public static By socialMedia_dynamicList_xpath(){
		return By.xpath("(//div[@id='details'])[1]//div[contains(@id,'data_')]");
	}
	
	public static By socialMediaHeading_textBox_id(){
		return By.id("detail_heading");
	}
	
	public static By socialMediaLink_textBox_id(){
		return By.id("detail_description");
	}
	
	public static By socialMediaAdd_button_id(){
		return By.id("add_detail_btn");
	}
	
	public static By socialMediaRemove_dynamicLink_linkText(){
		return By.linkText("REMOVE");
	}
	
	//////////////Assign New Test Popup////////////////
	
	public static By assignPopup_popup_id(){
		return By.id("assign_popup");
	}
	
	public static By proceed_button_xpath(){
		return By.xpath("//form[@id='assign_test_popup_form']//input[@class='button_style' and @value='PROCEED']");
	}
	
	public static By cancel_button_xpath(){
		return By.xpath("//form[@id='assign_test_popup_form']//input[@class='button_style' and @value='CANCEL']");
	}
}
