package SoluApp.SeleniumProofCase.pages_candidateProfile;

import org.openqa.selenium.By;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_sitewide.Sitewide_Actions;
import SoluApp.SeleniumProofCase.pages_sitewide.Sitewide_Elements_Actions;

import java.lang.reflect.Method;

import SoluApp.SeleniumProofCase.Constants;

public class CandidateProfile_Elements_Verify_EditMode{
	public static void verify() throws Exception{
		Sitewide_Elements_Actions.dashboard_headerLink_click();
		Sitewide_Actions.editCandidate_search(Constants.defaultCandidateName, Constants.defaultCandidateName);
		Method[] methods = CandidateProfile_Elements_EditMode.class.getMethods();
		for (Method m : methods){
			if( (m.getName().toLowerCase().contains("xpath") || m.getName().toLowerCase().contains("id")
					|| m.getName().toLowerCase().contains("linktext") || m.getName().toLowerCase().contains("classname") )
					&& (!m.getName().toLowerCase().contains("dynamic")) ){
				System.out.println("Checking element: " + m.getName());
				Utilities.assertEquals(Utilities.doesElementExist((By)m.invoke(CandidateProfile_Elements_EditMode.class)), true,
						"The element - " + m.getName() + " does not exist at - " + m.invoke(CandidateProfile_Elements_EditMode.class));
			}
		}
	}
}
