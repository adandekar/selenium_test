package SoluApp.SeleniumProofCase.pages_candidateProfile;

import org.openqa.selenium.By;

public class CandidateProfile_Elements_ViewMode {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	public static By heading_text_xpath(){
		return By.xpath("//h1[@class='profile_heading']");
	}
	
	public static By endRecruitment_button_id(){
		return By.id("recruitment_btn");
	}
	
	public static By communicationStatus_button_id(){
		return By.id("communication_status_btn");
	}
	
	public static By downloadResume_dynamicButton_id(){
		return By.id("download_resume_btn");
	}
	
	public static By editMode_button_id(){
		return By.id("edit_btn");
	}
	
	//////////////Candidate Profile Information/////////////////////
	
	public static By firstName_text_xpath(){
		return By.xpath("//span[@id='first_name']");
	}
	
	public static By lastName_text_xpath(){
		return By.xpath("//span[@id='last_name']");
	}
	
	public static By emailAddress_text_xpath(){
		return By.xpath("//span[@id='email_address']");
	}
	
	public static By phoneNumber_text_xpath(){
		return By.xpath("//span[@id='phone_number']");
	}
	
	public static By address_text_xpath(){
		return By.xpath("//span[@id='address']");
	}
	
	public static By city_text_xpath(){
		return By.xpath("//span[@id='address']");
	}
	
	public static By state_text_xpath(){
		return By.xpath("//span[@id='address']");
	}
	
	public static By zipCode_text_xpath(){
		return By.xpath("//span[@id='zip_code']");
	}
	
	public static By resourceType_text_xpath(){
		return By.xpath("//span[@id='resource_type']");
	}
	
	public static By level_text_xpath(){
		return By.xpath("//span[@id='level']");
	}
	
	public static By location_text_xpath(){
		return By.xpath("//span[@id='location']");
	}
	
	public static By recruiter_text_xpath(){
		return By.xpath("//span[@id='recruiters']");
	}
	
	public static By tags_text_id(){
		return By.id("tags");
	}
	
	///////////////Profile Pic/////////////////////
	
	public static By profilePicture_image_id(){
		return By.id("photo_display");
	}
	
	///////////////Assessments/////////////////////
	public static By assessmentCheckbox_checkBox_id(){
		return By.id("assessment_checkbox");
	}
	
	public static By sendSelected_button_id(){
		return By.id("send_selected_btn");
	}
	
	public static By sendThankYouEmail_button_id(){
		return By.id("thankyou_email_btn");
	}
	
	public static By thankYouPopup_popup_id(){
		return By.id("thankyou_popup");
	}
	
	public static By thankYouPopupSend_button_id(){
		return By.id("thankyou_send_btn");
	}
	
	public static By thankYouPopupCancel_button_id(){
		return By.id("thankyou_cancel_btn");
	}
	
	//////////////Assessment Data////////////////
	public static By assessmentBody_textArea_id(){
		return By.id("email_body");
	}
	
	public static By assessmentSubject_textBox_id(){
		return By.id("email_subject");
	}
	
	public static By assessmentFromEmail_textBox_id(){
		return By.id("from_email");
	}
	
	public static By assessmentAdminEmailBody_textArea_id(){
		return By.id("admin_email_body");
	}
	
	////////////////Social Media/////////////////
	
	public static By socialMedia_dynamicList_xpath(){
		return By.xpath("//div[@id='candidate_social_and_other_details']//div[contains(@id,'data_')]");
	}
	
	//////////////Comments/////////////////////
	
	public static By commentsText_textArea_id(){
		return By.id("candidate_comment_text");
	}
	
	public static By commentsList_dynamicList_xpath(){
		return By.xpath("//div[@id='candidate_comments']//div[contains(@id,'data_')]");
	}
	
	public static By commentText_dynamicText_xpath(){
		return By.xpath("//div[@class='detail_row_heading']//div[3]");
	}
	
	public static By addComment_button_id(){
		return By.id("add_comment_btn");
	}
	
	//////////////Assign New Test Popup////////////////
	
	public static By assignPopup_popup_id(){
		return By.id("assign_popup");
	}
	
	public static By proceed_button_id(){
		return By.id("popup_proceed_btn");
	}
	
	public static By cancel_button_id(){
		return By.id("popup_cancel_btn");
	}
}
