package SoluApp.SeleniumProofCase.pages_candidateProfile;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.pages_candidates.*;
import SoluApp.SeleniumProofCase.Utilities;

public class CandidateProfile_Actions {
	//TODO: java doc these
	public static void removeProfilePic(int pictureSize) throws Exception{
		CandidateProfile_Elements_Actions.removePhoto_link_click();
		CandidateProfile_Elements_Actions.save_button_click();
		Utilities.assertEquals(CandidateProfile_Elements_Actions.checkPhoto_image_check(pictureSize), false, "Candidate profile photo not properly removed");
	}
	
	/**
	 * 
	 * @param directory
	 */
	public static void addProfilePic(String directory, int pictureSize) throws Exception{
		CandidateProfile_Elements_Actions.uploadPhotoSendDirectory_link_fill(directory);
		CandidateProfile_Elements_Actions.save_button_click();
		Utilities.assertEquals(CandidateProfile_Elements_Actions.checkPhoto_image_check(pictureSize), true, "Candidate profile photo not properly added");
	}
	
	public static void addResume(String directory){
		CandidateProfile_Elements_Actions.uploadResumeDirectory_link_fill(directory);
		CandidateProfile_Elements_Actions.save_button_click();
		CandidateProfile_Elements_Actions.view_button_click();
		Utilities.assertEquals(Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.downloadResume_dynamicButton_id()), true,
				"Candidate resume not properly added");
	}
	
	public static void downloadResume() throws Exception{
		CandidateProfile_Elements_Actions.downloadResume_button_click();
//		Utilities.assertEquals(Utilities.isSMT(), false, "Did not open a separate page to download the pdf.");
//		Utilities.assertNotEquals(Utilities.driver.findElement(By.xpath("html/body/h1")).getText().toLowerCase(), "not found", "Resume not found on the server");
	}
	
	/**
	 * Find and return an assessment element
	 * @param driver
	 * @param assessmentName
	 * @return
	 */
	public static WebElement findAssessment(String assessmentName){//TODO: update these references and move the xpaths into elements substructure
		List<WebElement> elementList = Utilities.driver.findElements(By.xpath("//form[@name='candidate_assessments']//div[contains(@id,'data_') and not(contains(@id,'sub'))]"));
		System.out.println("list size: " + elementList.size());
		for(int i = 1; i <= elementList.size(); i++){
			String myText = Utilities.driver.findElement(By.xpath("//form[@name='candidate_assessments']//div[contains(@id,'data_') and not(contains(@id,'sub'))]["+i+"]//div[@class='detail_1']")).getText();
			System.out.println("Element text: " + myText);
			if(assessmentName.equalsIgnoreCase(myText)){return Utilities.driver.findElement(By.xpath("//form[@name='candidate_assessments']//div[@id='data_"+i+"' and not(contains(@id,'sub'))]"));}
		}
		return null;
	}
	
	public static void selectAssessment(String assessmentName) throws Exception {
		CandidateProfile_Actions.findAssessment(assessmentName).findElement(CandidateProfile_Elements_ViewMode.assessmentCheckbox_checkBox_id()).click();
	}
	
//	/**
//	 * Select the self assessment from the assessment list
//	 * @param driver
//	 * @param candidateName
//	 * @throws Exception
//	 */
//	public static void selfAssessment() throws Exception{
//		element = CandidateProfile_Actions.findAssessment("Self Assessment").findElement(CandidateProfile_Elements_ViewMode.assessmentCheckbox_checkBox_id());
//		element.click();
//	}
//	
//	/**
//	 * Select the math and logic assessment from the assessment list
//	 * @param driver
//	 * @param candidateName
//	 * @throws Exception
//	 */
//	public static void mathAndLogicAssessment(String candidateName) throws Exception{
//		element = CandidateProfile_Actions.findAssessment("Math and Logic Assessment").findElement(CandidateProfile_Elements_ViewMode.assessmentCheckbox_checkBox_id());
//		element.click();
//	}
//	
//	/**
//	 * Select the technical knowledge assessment from the assessment list
//	 * @param driver
//	 * @param candidateName
//	 * @throws Exception
//	 */
//	public static void technicalKnowledgeAssessment(String candidateName) throws Exception{
//		element = CandidateProfile_Actions.findAssessment("Technical Knowledge Assessment").findElement(CandidateProfile_Elements_ViewMode.assessmentCheckbox_checkBox_id());
//		element.click();
//	}
	
	/**
	 * Send the selected assessments.
	 * @Verification check if the assign test page is opened up
	 * @param driver
	 */
	public static void sendAssessments(){
		CandidateProfile_Elements_Actions.sendSelected_button_click();
		CandidateProfile_Elements_Actions.proceed_button_assignNewTest_click();
		
		Utilities.assertEquals(Sitewide_Elements_Verify.isAssignTest_text_siteWide_verify(), true, "Not assign test page");
	}
	
	/**
	 * Send a thank you email.
	 * @Verification None TODO: this should have a verification
	 * @param driver
	 * @param candidateName
	 * @throws Exception
	 */
	public static void sendThankYouEmail(String candidateName) throws Exception{
		Candidates_Actions.viewCandidate(candidateName);
		CandidateProfile_Elements_Actions.sendThankYouEmail_button_click();
		CandidateProfile_Elements_Actions.send_button_thankYouEmailPopup_click();
	}
	
	/**
	 * Find and return social media element.
	 * @param heading
	 * @param link
	 * @return
	 */
	public static WebElement findSocialMedia(String heading, String link){
		List<WebElement> elementList = CandidateProfile_Elements_Actions.getSocialMedia_list_get();
		for(WebElement myElement : elementList){
			if(heading.equalsIgnoreCase(myElement.findElement(Sitewide_Elements.detail1_dynamicList_classname()).getText())){
				if(link.equalsIgnoreCase(myElement.findElement(Sitewide_Elements.detail2_dynamicList_classname()).findElement(By.tagName("a")).getText())){
					return myElement;
				}
			}
		}
		return null;
	}
	
	public static WebElement findSocialMedia_EditMode(String heading, String link){
		List<WebElement> elementList = CandidateProfile_Elements_Actions.getSocialMedia_list_get();
		if(elementList == null){return null;}
//		for(int i = 0; i<elementList.size()*2; i = i + 2){
		for(WebElement myElement : elementList){
			System.out.println("MyElement Id: " + myElement.getAttribute("id"));
			System.out.println("Myelement heading: " + myElement.findElement( By.xpath(".//div[@class='detail_1']//input[1]")).getAttribute("value")
					+ " myElement link: " + myElement.findElement(By.xpath(".//div[@class='detail_2']//input[1]")).getAttribute("value"));
//			System.out.println("heading: " + heading + " link: " + link);
			if(heading.equalsIgnoreCase(myElement.findElement(By.xpath(".//div[@class='detail_1']//input[1]")).getAttribute("value"))){
				if(link.equalsIgnoreCase(myElement.findElement(By.xpath(".//div[@class='detail_2']//input[1]")).getAttribute("value"))){
					return myElement;
				}
			}
		}
		return null;
	}
	
	/**
	 * Add a social media site to the candidate profile page.
	 * @Verification check if the social media exists after being added
	 * @param heading
	 * @param link
	 */
	public static void addSocialMedia(String heading, String link){
		CandidateProfile_Elements_Actions.heading_textBox_fill(heading);
		CandidateProfile_Elements_Actions.link_textBox_fill(link);
		CandidateProfile_Elements_Actions.add_button_click();
		CandidateProfile_Elements_Actions.save_button_click();
		
		CandidateProfile_Elements_Actions.view_button_click();
		Utilities.assertNotEquals(findSocialMedia(heading, link), null, "Social media not added properly");
	}
	
	/**
	 * Remove a social media site from the candidate profile page.
	 * @param heading
	 * @param link
	 */
	public static void removeSocialMedia(String heading, String link) throws Exception{
		WebElement myElement = findSocialMedia_EditMode(heading, link);
//		Thread.sleep(5000);
		System.out.println("myElement: " + myElement);
		System.out.println("element inner html: " + myElement.getAttribute("innerHTML"));
		myElement.findElement(CandidateProfile_Elements_EditMode.socialMediaRemove_dynamicLink_linkText()).click();
//		Thread.sleep(5000);
		CandidateProfile_Elements_Actions.save_button_click();
		Utilities.assertEquals(findSocialMedia_EditMode(heading, link), null, "Social media was not properly removed.");
	}
	
	/**
	 * Find and return comment element from the candidate profile page.
	 * @param comment
	 * @return
	 */
	public static WebElement findComment(String comment){
		List<WebElement> elementList = CandidateProfile_Elements_Actions.getComments_list_get();
		if(elementList == null){return null;}
		for(int i = 0; i<elementList.size(); i++){//TODO: make this for loop like the find social for loop
			if(comment.equalsIgnoreCase(Utilities.driver.findElement(By.xpath(
					"(//div[@id='details'])[2]//div[contains(@id,'data_')]["+(i+1)+"]//div[@class='detail_row_heading']//div[3]")).getText())){
				System.out.println("Returning comment id: " + elementList.get(i).getAttribute("id"));
				System.out.println("Returning comment inner html: " + elementList.get(i).getAttribute("innerHTML"));
				return elementList.get(i);
			}
		}
		return null;
	}
	
	/**
	 * Add comment to the candidate profile page.
	 * @param comment
	 */
	public static void addComment(String comment){
		CandidateProfile_Elements_Actions.comment_textArea_fill(comment);
		CandidateProfile_Elements_Actions.addComment_button_click();
		Utilities.assertNotEquals(findComment(comment), null, "Comment not properly added.");
	}
	
	/**
	 * Edit comment in the candidate profile page
	 * @param comment
	 * @throws Exception
	 */
	public static void editComment(String oldComment, String newComment) throws Exception{
		WebElement myElement = findComment(oldComment);
		myElement.findElement(Sitewide_Elements.editListElement_dynamicList_linkText()).click();
		myElement = Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.commentsText_textArea_id());
		Utilities.clearElement(myElement);
		myElement.sendKeys(newComment);
		CandidateProfile_Elements_Actions.addComment_button_click();
		Utilities.assertEquals(findComment(oldComment), null, "Comment not properly editted, found the old comment.");
		Utilities.assertNotEquals(findComment(newComment), null, "Comment not properly editted, can't find new comment.");
	}
	
	/**
	 * Remove comment from the candidate profile page.
	 * @param comment
	 */
	public static void removeComment(String comment) throws Exception{
		WebElement myElement = findComment(comment);
		myElement.findElement(Sitewide_Elements.removeListElement_dynamicList_linkText()).click();
		Utilities.acceptAlert();
		Utilities.assertEquals(findComment(comment), null, "Comment not properly removed.");
	}
	
	/*		Get Information		*/
	
	/**
	 * Navigate to the candidate profile page and return the candidate email address.
	 * @param candidateName
	 * @return
	 * @throws Exception
	 */
	public static String getEmailAddress(String candidateName) throws Exception{
		Candidates_Actions.viewCandidate(candidateName);
		return CandidateProfile_Elements_Actions.getEmailAddress_textBox_get();
		
	}
	
	/**
	 * Navigate to the candidate profile page and return the candidate email user name.
	 * @param candidateName
	 * @return
	 * @throws Exception
	 */
	public static String getUserName(String candidateName) throws Exception{
		Candidates_Actions.viewCandidate(candidateName);
		return CandidateProfile_Elements_Actions.getUserName_text_get();
	}
	
	/**
	 * Fill out or change all the candidate Information
	 * @Assumptions in candidate profile page, in edit mode
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param address
	 * @param city
	 * @param state
	 * @param zipCode
	 * @param resourceType
	 * @param level
	 * @param location
	 * @param recruiter
	 * @param tags
	 */
	public static void fillCandidateInformation(String firstName, String lastName, String email, String phoneNumber, String address, String city, String state, String zipCode,
			String resourceType, String level, String location, String recruiter, String[] tags) throws Exception{
		CandidateProfile_Elements_Actions.setFirstName_textBox_set(firstName);
		CandidateProfile_Elements_Actions.setLastName_textBox_set(lastName);
		CandidateProfile_Elements_Actions.setEmailAddress_textBox_set(email);
		CandidateProfile_Elements_Actions.setPhoneNumber_textBox_set(phoneNumber);
		CandidateProfile_Elements_Actions.setAddress_textBox_set(address);
		CandidateProfile_Elements_Actions.setCity_textBox_set(city);
		CandidateProfile_Elements_Actions.setState_textBox_set(state);
		CandidateProfile_Elements_Actions.setZipCode_textBox_set(zipCode);
		CandidateProfile_Elements_Actions.setResourceType_textBox_set(resourceType);
		CandidateProfile_Elements_Actions.setLevel_textBox_set(level);
		CandidateProfile_Elements_Actions.setLocation_textBox_set(location);
		CandidateProfile_Elements_Actions.setRecruiter_textBox_set(recruiter);
		CandidateProfile_Elements_Actions.setTags_textBox_set(tags);
	}
	
	/////////////////////Verify Candidate Information/////////////////////////////
	/**
	 * Verify if on a candidate profile page and the name is correct.
	 * @Assumptions: that the current page is a candidate profile page and it is in view mode.
	 * @param fullName
	 * @throws Exception
	 */
	public static void verifyCandidate_viewMode(String fullName) throws Exception{
		System.out.println("Running verifyCandidate_viewMode");
		String firstName;
		String lastName;
		firstName = fullName.split(" ")[0];
		lastName = fullName.split(" ")[1];
		Utilities.assertEquals(Sitewide_Elements_Verify.isCandidateProfile_text_siteWide_verify(), true, "Not candidate profile page");
		
		Utilities.assertEquals(firstName, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.firstName_text_xpath()).getText(),
				"First name is incorrect");
		Utilities.assertEquals(lastName, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.lastName_text_xpath()).getText(),
				"Last name is incorrect");
	}
	
	/**
	 * Verify if on a candidate profile page and the name is correct.
	 * @Assumptions: that the current page is a candidate profile page and it is in edit mode.
	 * @param fullName
	 * @throws Exception
	 */
	public static void verifyCandidate_editMode(String fullName) throws Exception{
		System.out.println("Running verifyCandidate_editMode");
		String firstName;
		String lastName;
		firstName = fullName.split(" ")[0];
		lastName = fullName.split(" ")[1];
		Utilities.assertEquals(Sitewide_Elements_Verify.isCandidateProfile_text_siteWide_verify(), true, "Not candidate profile page");
		
		Utilities.assertEquals(firstName, CandidateProfile_Elements_Actions.getFirstName_textBox_get(),
				"First name is incorrect");
		Utilities.assertEquals(lastName, CandidateProfile_Elements_Actions.getLastName_textBox_get(),
				"Last name is incorrect");
	}

	public static void verifyCandidate(String firstName, String lastName, String email, String phoneNumber, String recruiter, String resourceType) throws Exception{
		System.out.println("Running verifyCandidate");
		Candidates_Actions.viewCandidate(firstName+" "+lastName);
		Utilities.assertEquals(Sitewide_Elements_Verify.isCandidateProfile_text_siteWide_verify(), true, "Not candidate profile page");
		
		Utilities.assertEquals(firstName, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.firstName_text_xpath()).getText(),
				"First name is incorrect");
		Utilities.assertEquals(lastName, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.lastName_text_xpath()).getText(),
				"Last name is incorrect");
		Utilities.assertEquals(email, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.emailAddress_text_xpath()).getText(),
				"Email address is incorrect");
		Utilities.assertEquals(phoneNumber, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.phoneNumber_text_xpath()).getText(),
				"Phone number is incorrect");
		Utilities.assertEquals(recruiter, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.recruiter_text_xpath()).getText(),
				"Recruiter is incorrect");
		Utilities.assertEquals(resourceType, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.resourceType_text_xpath()).getText(),
				"Resource type is incorrect");
	}
	
	public static void verifyCandidate(String firstName, String lastName, String email, String phoneNumber, String recruiter, String resourceType,
			String address, String city, String state, String zipCode, String level, String location, String tags[]) throws Exception{
		verifyCandidate(firstName, lastName, email, phoneNumber, recruiter, resourceType);
		
		Utilities.assertEquals(address, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.address_text_xpath()).getText().split(", ")[0],
				"Address is incorrect");
		Utilities.assertEquals(city, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.city_text_xpath()).getText().split(", ")[1],
				"City type is incorrect");
		Utilities.assertEquals(state, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.state_text_xpath()).getText().split(", ")[2],
				"State is incorrect");
		Utilities.assertEquals(zipCode, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.zipCode_text_xpath()).getText(),
				"ZipCode type is incorrect");
		Utilities.assertEquals(level, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.level_text_xpath()).getText(),
				"Level is incorrect");
		Utilities.assertEquals(location, Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.location_text_xpath()).getText(),
				"Location type is incorrect");
		
		for(int i = 0; i < tags.length; i++){
			Utilities.assertEquals(tags[i], Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.tags_text_id()).getText().split(", ")[i],
					"a tag is incorrect");
		}
	}
}
