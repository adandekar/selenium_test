package SoluApp.SeleniumProofCase;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import SoluApp.SeleniumProofCase.Utilities;

public class GuerillaMail {
	public static WebElement element = null;
	public static String userName = "";
	
	public static void load(String value) throws Exception{
		System.out.println("Loading the email of: " + value);
		Utilities.driver.get("https://www.guerrillamail.com/inbox/" + value);
		userName = value;
		Thread.sleep(11000);
	}
	
	public static void getEmail() throws Exception{
		element = Utilities.driver.findElement(By.id("email_list"));
		System.out.println("creating list");
		List<WebElement> elements = Utilities.driver.findElements(By.xpath("//tbody[@id='email_list']//tr"));
		for(int i = 0; i<elements.size(); i++){
			System.out.println("From: " + Utilities.driver.findElement(By.xpath("//tbody[@id='email_list']//tr["+(i+1)+"]//td[2]")).getText());
			System.out.println("Subject: " + Utilities.driver.findElement(By.xpath("//tbody[@id='email_list']//tr["+(i+1)+"]//td[3]")).getText());
		}
		System.out.println("done");
	}
	
	public static void checkForAssessment(String subjectKeyword, String[] bodyKeywords) throws Exception{
		Utilities.assertEquals(isAssessment(subjectKeyword, bodyKeywords), true, "Assessment email not found.");
	}
	
	public static void checkForThankYou() throws Exception{
		Utilities.assertEquals(GuerillaMail.findThankYouEmail(), true, "Thank you email does not exist.");
	}
	
	public static boolean checkSender(WebElement myElement){
		String from = myElement.findElement(By.xpath(".//td[2]")).getText().toLowerCase();
		if (from.equals("solu-dev@solutechnology.com")){return true;}
		return false;
	}
	
	public static boolean isAssessment(String subjectKeyword, String[] bodyKeywords) throws Exception{
		List<WebElement> elements = GuerillaMail.getEmailList();
		
		if (elements.size() == 0 || elements.get(0).getAttribute("class").equals("no_emails")){
			System.out.println("ERROR!! No emails");  return false;}
		
		
//		String subject = "";
		String body = "";
		boolean hasKeywords = true;
		for(int i = 0; i<elements.size(); i++){
			String subject = GuerillaMail.getNodeText(Utilities.driver.findElement(By.xpath("//tbody[@id='email_list']//tr["+(i+1)+"]//td[3]")));
			if(!subject.contains(subjectKeyword)){continue;}
			if(!GuerillaMail.checkSender(Utilities.driver.findElement(By.xpath("//tbody[@id='email_list']//tr["+(i+1)+"]")))){continue;}
			Utilities.driver.findElement(By.xpath("//tbody[@id='email_list']//tr["+(i+1)+"]")).click();//open up email

			GuerillaMail.isEmailOpen();
			body = Utilities.driver.findElement(By.xpath("//div[@id='display_email']//div[@class='email_body']")).getText();
			hasKeywords = true;
			for(String keyword : bodyKeywords){
				if(!body.toLowerCase().contains(keyword.toLowerCase())){hasKeywords = false;}	
			}
			if(hasKeywords){load(userName);System.out.println("It has all the keywords! " + body);return true;}
			else{load(userName);}
		}
		System.out.println("Email not found at all");
		return false;
	}
	
	public static boolean findThankYouEmail(){
		List<WebElement> elements = GuerillaMail.getEmailList();
		
		String subject = "";
		for(int i = 0; i<elements.size(); i++){
//			subject = GuerillaMail.getEmailSubject(Utilities.driver.findElement(By.xpath("//tbody[@id='email_list']//tr["+(i+1)+"]")));
			subject = Utilities.driver.findElement(By.xpath("//tbody[@id='email_list']//tr["+(i+1)+"]")).getText();
			System.out.println("Thank you subject: " + subject);
			if(subject.toLowerCase().contains("thank you")){return true;}
		}
		
		return false;
	}
	
	public static List<WebElement> getEmailList(){
		element = Utilities.driver.findElement(By.id("email_list"));
		if(Utilities.doesElementExist(By.xpath("//tbody[@id='email_list']//tr"))){
		return element.findElements(By.tagName("tr"));}
		else{return null;}
	}
	
	public static void deleteMail() throws Exception {
		List<WebElement> elements = GuerillaMail.getEmailList();
		System.out.println("elements size: " + elements.size());
		Utilities.assertNotEquals(elements.size(), 0, "There are no emails to delete");
		Utilities.assertNotEquals(elements.get(0).getAttribute("class"), "no_emails", "There are no emails to delete");
		
		for(int i = 0; i<elements.size(); i++){
			Utilities.driver.findElement(By.xpath("//tbody[@id='email_list']//tr["+(i+1)+"]//td[1]//input[1]")).click();
			System.out.println("Selecting a candidate to delete");
		}

		Utilities.driver.findElement(By.id("del_button")).click();
	}
	

	public static String getNodeText(WebElement element) {
		String text = element.getText();
		boolean isOwnText = true;//do not remove the first text because that is the elements text
		List<WebElement> elementList = element.findElements(By.xpath(".//*"));
		for (WebElement child : elementList) {
			if(!isOwnText){
				text = text.replaceFirst(child.getText(), "");
			}else{isOwnText=false;}
		}
		return text;
	}
	
	public static void isEmailOpen(){
		//check if an email is open, ie body is fully visible
		element = Utilities.driver.findElement(By.id("display_email"));
		Utilities.assertEquals(element.isDisplayed(), true, "Email body is not open");
	}
}
