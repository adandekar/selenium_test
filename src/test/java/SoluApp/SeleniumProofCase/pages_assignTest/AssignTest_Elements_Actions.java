package SoluApp.SeleniumProofCase.pages_assignTest;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import SoluApp.SeleniumProofCase.Utilities;

public class AssignTest_Elements_Actions {
	private static WebElement element = null;
	
	public static WebElement findAssessment(String assessmentName){
		List<WebElement> elementList = Utilities.driver.findElements(By.xpath("//div[@id='details_new_assessment']//div[contains(@id,'data_')]"));
		for(WebElement myElement : elementList){
			if(Utilities.getText(myElement.findElement(AssignTest_Elements.assessmentName_text_xpath())).equalsIgnoreCase(assessmentName)){return myElement;}
		}
		return null;
	}
	
	public static void selectCandidate_dropDown_selectValue(String assessmentName, String candidateName){
		if (candidateName != ""){
			element = findAssessment(assessmentName).findElement(By.xpath(".//select[contains(@id,'_candidate')]"));
			Select select = new Select(element);
			select.selectByVisibleText(candidateName);
		}
	}
	
	public static void assignAssessment_button_click(String assessmentName){
		element = findAssessment(assessmentName);
		element.findElement(AssignTest_Elements.assign_button_xpath()).click();
	}
	
//	public static void selfAssessmentCandidate_dropDown_selectValue(String value){
//		
//	}
//	
//	public static void selfAssessmentAssign_button_click(){
//		element = Utilities.driver.findElement(AssignTest_Elements.selfAssessmentAssign_button_xpath());
//		element.click();
//	}
//	
//	public static void mathAndLogicAssessmentCandidate_dropDown_selectValue(String value){
//		element = Utilities.driver.findElement(AssignTest_Elements.mathAndLogicAssessmentCandidate_dropDown_xpath());
//		Select select = new Select(element);
//		select.selectByVisibleText(value);
//	}
//	
//	public static void mathAndLogicAssessmentAssign_button_click(){
//		element = Utilities.driver.findElement(AssignTest_Elements.mathAndLogicAssessmentAssign_button_xpath());
//		element.click();
//	}
//	
//	public static void technicalKnowledgeAssessmentCandidate_dropDown_selectValue(String value){
//		element = Utilities.driver.findElement(AssignTest_Elements.technicalKnowledgeAssessmentCandidate_dropDown_xpath());
//		Select select = new Select(element);
//		select.selectByVisibleText(value);
//	}
//	
//	public static void technicalKnowledgeAssessmentAssign_button_click(){
//		element = Utilities.driver.findElement(AssignTest_Elements.technicalKnowledgeAssessmentAssign_button_xpath());
//		element.click();
//	}
}
