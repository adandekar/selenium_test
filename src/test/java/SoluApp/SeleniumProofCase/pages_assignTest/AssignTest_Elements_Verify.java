package SoluApp.SeleniumProofCase.pages_assignTest;

import java.lang.reflect.Method;

import org.openqa.selenium.By;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_sitewide.Sitewide_Elements_Actions;

public class AssignTest_Elements_Verify {
	public static void verify() throws Exception{
		Sitewide_Elements_Actions.assignTest_headerLink_click();
		Method[] methods = AssignTest_Elements.class.getMethods();
		for (Method m : methods){
			if(m.getName().toLowerCase().contains("xpath") || m.getName().toLowerCase().contains("id")
					|| m.getName().toLowerCase().contains("linktext") || m.getName().toLowerCase().contains("classname")){
				System.out.println("Checking element: " + m.getName());
				Utilities.assertEquals(Utilities.doesElementExist((By)m.invoke(AssignTest_Elements.class)), true,
						"The element - " + m.getName() + " does not exist at - " + m.invoke(AssignTest_Elements.class));
			}
		}
	}
}
