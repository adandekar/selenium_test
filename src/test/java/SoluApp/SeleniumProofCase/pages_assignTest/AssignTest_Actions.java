package SoluApp.SeleniumProofCase.pages_assignTest;

import SoluApp.SeleniumProofCase.pages_candidateProfile.*;
import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.Utilities;

public class AssignTest_Actions {
	
	/**
	 * Select candidate to assign assessment to and click "assign" but to not send the assessment
	 * @param assessmentName
	 * @param candidateName
	 */
	public static void assignAssessment(String assessmentName, String candidateName){
		if (!candidateName.equals("")){
			AssignTest_Elements_Actions.selectCandidate_dropDown_selectValue(assessmentName, candidateName);
			AssignTest_Elements_Actions.assignAssessment_button_click(assessmentName);
			Utilities.assertEquals(Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.assignPopup_popup_id()), true, "The popup does not exist");
		}else{
			AssignTest_Elements_Actions.assignAssessment_button_click(assessmentName);
			Utilities.assertEquals(Sitewide_Elements_Verify.isAssignTest_text_siteWide_verify(), true, "Not assign test page");
		}
	}
	
	/**
	 * Select candidate, assign assessment and send assessment
	 * @param assessmentName
	 * @param candidateName
	 */
	public static void sendAssessment(String assessmentName, String candidateName){//TODO: javadoc this
		if (!candidateName.equals("")){
			AssignTest_Elements_Actions.selectCandidate_dropDown_selectValue(assessmentName, candidateName);
			AssignTest_Elements_Actions.assignAssessment_button_click(assessmentName);
			Utilities.assertEquals(Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.assignPopup_popup_id()), true, "The popup does not exist");
			CandidateProfile_Elements_Actions.proceed_button_assignNewTest_click();
		}else{
			AssignTest_Elements_Actions.assignAssessment_button_click(assessmentName);
			Utilities.assertEquals(Sitewide_Elements_Verify.isAssignTest_text_siteWide_verify(), true, "Not assign test page");
		}
	}
	
//	/**
//	 * Assign self assessment from assign test page.
//	 * @Verification if candidate exists: check if the assign popup opens up.
//	 * 				 if candidate is "": check that the assign button did not change the page
//	 * @param driver
//	 * @param value
//	 */
//	public static void selfAssessment(String value){
//		if (!value.equals("")){
//			AssignTest_Elements_Actions.selfAssessmentCandidate_dropDown_selectValue(value);
//			AssignTest_Elements_Actions.selfAssessmentAssign_button_click();
//			Utilities.assertEquals(Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.assignPopup_popup_id()), true, "The popup does not exist");
//			CandidateProfile_Elements_Actions.proceed_button_assignNewTest_click();
//		}else{
//			AssignTest_Elements_Actions.selfAssessmentAssign_button_click();
//			Utilities.assertEquals(Sitewide_Elements_Verify.isAssignTest_text_siteWide_verify(), true, "Not assign test page");
//		}
//	}
//	
//	/**
//	 * Assign math and logic assessment from assign test page.
//	 * @Verification if candidate exists: check if the assign popup opens up.
//	 * 				 if candidate is "": check that the assign button did not change the page
//	 * @param driver
//	 * @param value
//	 */
//	public static void mathAndLogicAssessment(String value){
//		if (!value.equals("")){
//			AssignTest_Elements_Actions.mathAndLogicAssessmentCandidate_dropDown_selectValue(value);
//			AssignTest_Elements_Actions.mathAndLogicAssessmentAssign_button_click();
//			Utilities.assertEquals(Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.assignPopup_popup_id()), true, "The popup does not exist");
//			CandidateProfile_Elements_Actions.proceed_button_assignNewTest_click();
//		}else{
//			AssignTest_Elements_Actions.mathAndLogicAssessmentAssign_button_click();
//			Utilities.assertEquals(Sitewide_Elements_Verify.isAssignTest_text_siteWide_verify(), true, "Not assign test page");
//		}
//	}
//	
//	/**
//	 * Assign technical knowledge assessment from the assign test page.
//	 * @Verification if candidate exists: check if the assign popup opens up.
//	 * 				 if candidate is "": check that the assign button did not change the page
//	 * @param driver
//	 * @param value
//	 */
//	public static void technicalKnowledgeAssessment(String value){
//		if (!value.equals("")){
//			AssignTest_Elements_Actions.technicalKnowledgeAssessmentCandidate_dropDown_selectValue(value);
//			AssignTest_Elements_Actions.technicalKnowledgeAssessmentAssign_button_click();
//			Utilities.assertEquals(Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.assignPopup_popup_id()), true, "The popup does not exist");
//			CandidateProfile_Elements_Actions.proceed_button_assignNewTest_click();
//		}else{
//			AssignTest_Elements_Actions.technicalKnowledgeAssessmentAssign_button_click();
//			Utilities.assertEquals(Sitewide_Elements_Verify.isAssignTest_text_siteWide_verify(), true, "Not assign test page");
//		}
//	}
}
