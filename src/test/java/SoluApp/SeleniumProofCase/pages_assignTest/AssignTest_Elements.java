package SoluApp.SeleniumProofCase.pages_assignTest;

import org.openqa.selenium.By;

public class AssignTest_Elements {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	public static By selectCandidate_dropDown_xpath(){
		return By.xpath(".//form[contains(@id,'assign_form')]//select[@name='id']");
	}
	
	public static By assign_button_xpath(){
		return By.xpath(".//input[contains(@id,'assign_')]");
	}
	
	public static By assessmentName_text_xpath(){
		return By.xpath(".//div[@class='detail_1']");
	}
	
	//self: //div[@id='data_1']
	//math: //div[@id='data_2']
	//tech: //div[@id='data_3']
}
