package SoluApp.SeleniumProofCase.pages_sitewide;

import org.openqa.selenium.By;

public class Sitewide_Elements {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	public static By googleLogin_button_xpath(){
		return By.xpath("//div[@class='app_base']//a[@class='authenticate_button']");
	}
	
	public static By googleAuthEmail_textBox_id(){
		return By.id("Email");
	}
	
	public static By googleAuthPassword_textBox_id(){
		return By.id("Passwd");
	}
	
	public static By googleAuthNext_button_id(){
		return By.id("next");
	}
	
	public static By googleAuthSignIn_button_id(){
		return By.id("signIn");
	}
	
	public static By pageTitle_text_xpath(){
		//the header/title of the page, not the tab title
		return By.xpath("//div[@class='top_panel']//h1[1]");
	}
	
	////////////////////HEADER LINKS//////////////////////
	
	public static By dashboard_headerLink_xpath(){
		return By.id("header_admin_link_dashboard");
	}
	
	public static By candidates_headerLink_xpath(){
		return By.id("header_admin_link_candidates");
	}
	
	public static By assignTest_headerLink_xpath(){
		return By.id("header_admin_link_assign");
	}
	
	public static By sgSettings_headerLink_xpath(){
		return By.id("header_admin_link_sg_settings");
	}
	
	public static By smSettings_headerLink_xpath(){
		return By.id("header_admin_link_sm_settings");
	}
	
	public static By userManagement_headerLink_xpath(){
		return By.id("header_admin_link_users");
	}
	
	
	////////////////////Search Box///////////////////////
	
	public static By searchBox_textBox_id(){
		return By.id("search_keyword");
	}
	
	public static By search_button_id(){
		return By.id("search_btn");
	}
	
	public static By errorMessage_dynamicText_xpath(){
		return By.xpath("//div[@class='error_message']");
	}
	
	////////////////////List Control///////////////////////
	public static By getList_dynamicList_xpath(){
		return By.xpath("//div[@id='details']//div[contains(@id,'data_')]");
	}
	
	public static By viewListElement_dynamicList_linkText(){
		return By.linkText("VIEW");
	}
	
	public static By editListElement_dynamicList_linkText(){
		return By.linkText("EDIT");
	}
	
	public static By removeListElement_dynamicList_linkText(){
		return By.linkText("REMOVE");
	}
	
	public static By detail1_dynamicList_classname(){
		return By.className("detail_1");
	}
	
	public static By detail2_dynamicList_classname(){
		return By.className("detail_2");
	}
}
