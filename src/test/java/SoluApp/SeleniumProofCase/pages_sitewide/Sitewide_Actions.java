package SoluApp.SeleniumProofCase.pages_sitewide;

import java.util.List;

import org.openqa.selenium.WebElement;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_candidates.Candidates_Actions;
import SoluApp.SeleniumProofCase.pages_candidates.Candidates_Elements;
import SoluApp.SeleniumProofCase.pages_candidates.Candidates_Elements_Actions;

public class Sitewide_Actions {
	private static WebElement element = null;
	
	/**
	 * Reset the page to the default SMT url
	 * @Assumptions: the url is hard-coded
	 * @param driver
	 */
	public static void resetPage(){
		Utilities.driver.get("http://teambox.solu.com/html/SM_Source_Code/index.php");
	}
	
	/**
	 * Sign-in with google authentication.
	 * @Verification Checks the title of the page.
	 * @Assumptions: the page title is hard coded and assumed
	 * @param driver
	 * @param email
	 * @param password
	 */
	public static void googleAuth(String email, String password) throws Exception{
		Sitewide_Elements_Actions.googleAuthEmail_textBox_fill(email);
		Sitewide_Elements_Actions.next_button_click();
		Sitewide_Elements_Actions.googleAuthPasswd_textBox_sitewide_fill(password);
		Sitewide_Elements_Actions.signIn_button_click();
		Thread.sleep(3000);
//		Utilities.driver.wait(3000);
		Utilities.assertEquals(Utilities.driver.getTitle(), "SOLÜ - Staffing Manager", "Site Title is incorrect");
	}
	
	//////////////////Search Box/////////////////////////
	/**
	 * Fill the search box with a value and click search
	 * @Verification None
	 * @param driver
	 * @param searchTerm
	 */
	public static void search(String searchTerm){
		Sitewide_Elements_Actions.searchBox_textBox_fill(searchTerm);
		Sitewide_Elements_Actions.search_button_click();
	}
	
	/**
	 * Search for, and return the element of a candidate.
	 * @Verification None
	 * @param driver
	 * @param candidateName
	 * @return
	 */
	public static WebElement searchCandidate_search(String candidateName, String searchTerm){
		List<WebElement> myList = searchCandidates(searchTerm);
		
		for(WebElement currentElement:myList){
			String elementName = extractCandidateName(currentElement.findElement(Sitewide_Elements.detail1_dynamicList_classname()).getText());
			if(elementName.toLowerCase().equals(candidateName.toLowerCase())){
				return currentElement;
			}
		}
		return null;
	}
	
	/**
	 * Searches for, and clicks the view link of a candidate.
	 * @Verification Check view/edit mode button for its current state.
	 * @param driver
	 * @param candidateName
	 * @throws Exception
	 */
	public static void viewCandidate_search(String candidateName, String searchTerm) throws Exception{
		WebElement myElement = searchCandidate_search(candidateName, searchTerm);
		Utilities.assertNotEquals(myElement, null, "Candidate does not exist");
		myElement.findElement(Sitewide_Elements.viewListElement_dynamicList_linkText()).click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isViewMode_buttonId_candidateProfile_verify(candidateName), true, "Not in view mode.");
	}
	
	/**
	 * Searches for, and clicks the edit link of a candidate.
	 * @Verification Check view/edit mode button for its current state.
	 * @param candidateName
	 * @throws Exception
	 */
	public static void editCandidate_search(String candidateName, String searchTerm) throws Exception{
		WebElement myElement = searchCandidate_search(candidateName, searchTerm);
		Utilities.assertNotEquals(myElement, null, "Candidate does not exist");
		myElement.findElement(Sitewide_Elements.editListElement_dynamicList_linkText()).click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isEditMode_buttonId_candidateProfile_verify(candidateName), true, "Not in edit mode.");
	}
	
	/**
	 * Searches for, and removes a candidate.
	 * @Assumptions Remove link reads "REMOVE".
	 * @Verification Check for candidate removed message and check that the candidate no longer exists.
	 * @param candidateName
	 * @param customMaxWaitTime
	 * @throws Exception
	 */
	public static void removeCandidate_search(String candidateName, String searchTerm, int customMaxWaitTime) throws Exception{
		WebElement myElement = searchCandidate_search(candidateName, searchTerm);
		Utilities.assertNotEquals(myElement, null, "Candidate does not exist");
		myElement.findElement(Sitewide_Elements.removeListElement_dynamicList_linkText()).click();
		Utilities.acceptAlert(customMaxWaitTime);
		Utilities.acceptAlert(customMaxWaitTime);
		Utilities.assertEquals(Utilities.doesElementExist(Sitewide_Elements.errorMessage_dynamicText_xpath()), true, "Candidate removed error message did not display properly");
		Utilities.assertEquals(Candidates_Actions.findCandidate_candidates(candidateName), null, "Candidate was not properly removed");
	}
	
	/*		Utility Functions		*/
	/** Removes the number before a candidate's name in any list view.
	 * @Assumptions each candidate only has a single space in their full name, between their first name and last name.
	 * @Verification None
	 * @param value A string with the Candidate name proceeded by a unwanted number*/
	public static String extractCandidateName(String value){
		String[] elementNameArray = value.toLowerCase().split(" ");
		String elementName = elementNameArray[1];
		for(int i = 2; i < elementNameArray.length; i++){
			//correctly piece together the candidate name
			elementName += (" " + elementNameArray[i]);
		}
		return elementName;
	}
	
	
	/**
	 * Returns the list of candidates from a search query.
	 * @Verification None
	 * @param searchTerm
	 * @return
	 */
	public static List<WebElement> searchCandidates(String searchTerm){
		search(searchTerm);
		Utilities.assertEquals(Utilities.doesElementExist(Sitewide_Elements.getList_dynamicList_xpath()), true, "Candidate does not exist");
		return Utilities.driver.findElements(Sitewide_Elements.getList_dynamicList_xpath());
	}
	
	/**
	 * Fill out the create candidate popup with all the available information.
	 * @Verification check if the popup is open
	 * @param firstName
	 * @param lastName
	 * @param email
	 * @param phoneNumber
	 * @param recruiter
	 * @param resourceType
	 */
	public static void fillCandidateInformation(String firstName, String lastName, String email, String phoneNumber, String recruiter,
			String resourceType) {
		element = Utilities.driver.findElement(Candidates_Elements.candidatePopup_popup_id());
		Utilities.assertEquals(element.isDisplayed(), true, "Popup failed to open");

		Candidates_Elements_Actions.firstName_textBox_createCandidateInstance_fill(firstName);

		Candidates_Elements_Actions.lastName_textBox_createCandidateInstance_fill(lastName);

		Candidates_Elements_Actions.email_textBox_createCandidateInstance_fill(email);

		Candidates_Elements_Actions.phone_textBox_createCandidateInstance_fill(phoneNumber);

		Candidates_Elements_Actions.recruiter_dropDown_createCandidateInstance_selectValue(recruiter);

		Candidates_Elements_Actions.resourceType_dropDown_createCandidateInstance_selectValue(resourceType);
	}
	
	/**
	 * Save the filled out candidate information
	 * @Verification check that the create candidate popup is open and that no error message is generated
	 * @Assumptions Error message means candidate already exists
	 * @param driver
	 * @param isValidInput
	 * @param isUnique
	 */
	public static void save_createCandidateInstance(boolean isValidInput, boolean isUnique) {
		Candidates_Elements_Actions.save_button_createCandidateInstance_click();
		Utilities.assertEquals(!Utilities.isDisplayed(Candidates_Elements.candidatePopup_popup_id()), isValidInput, "Unexpected input");
		if(!isValidInput){Candidates_Elements_Actions.cancel_button_createCandidateInstance_click();}
		Utilities.assertEquals((Utilities.doesElementExist(Sitewide_Elements.errorMessage_dynamicText_xpath()) && isUnique), false, "Candidate already exists");
	}
}
