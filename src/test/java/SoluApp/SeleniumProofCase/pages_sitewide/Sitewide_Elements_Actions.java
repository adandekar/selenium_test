package SoluApp.SeleniumProofCase.pages_sitewide;

import SoluApp.SeleniumProofCase.Utilities;

public class Sitewide_Elements_Actions {
	public static void googleLogin_button_click(){
		Utilities.click(Sitewide_Elements.googleLogin_button_xpath());
	}
	
	public static void googleAuthEmail_textBox_fill(String value){
		Utilities.sendKeys(Sitewide_Elements.googleAuthEmail_textBox_id(), value);
	}
	
	public static void googleAuthPasswd_textBox_sitewide_fill(String value){
		Utilities.sendKeys(Sitewide_Elements.googleAuthPassword_textBox_id(), value);
	}
	
	public static void next_button_click(){
		Utilities.click(Sitewide_Elements.googleAuthNext_button_id());
	}
	
	public static void signIn_button_click(){
		Utilities.click(Sitewide_Elements.googleAuthSignIn_button_id());
	}
	
	public static void reset_headerLink_click(){
		Utilities.click(Sitewide_Elements.dashboard_headerLink_xpath());
	}
	
	/////////////////HEADER LINKS/////////////////////
	
	public static void dashboard_headerLink_click(){
		Utilities.click(Sitewide_Elements.dashboard_headerLink_xpath());
		Utilities.assertEquals(Sitewide_Elements_Verify.isDashboard_text_siteWide_verify(), true, "Dashboard link failed");
	}
	
	public static void candidates_headerLink_click(){
		Utilities.click(Sitewide_Elements.candidates_headerLink_xpath());
		Utilities.assertEquals(Sitewide_Elements_Verify.isCandidates_text_siteWide_verify(), true, "candidates link failed");
	}
	
	public static void assignTest_headerLink_click(){
		Utilities.click(Sitewide_Elements.assignTest_headerLink_xpath());
		Utilities.assertEquals(Sitewide_Elements_Verify.isAssignTest_text_siteWide_verify(), true, "Assign test link failed");
	}
	
	public static void sgSettings_headerLink_click(){
		Utilities.click(Sitewide_Elements.sgSettings_headerLink_xpath());
		Utilities.assertEquals(Sitewide_Elements_Verify.isSGSettings_text_siteWide_verify(), true, "SGSettings link failed");
	}
	
	public static void smSettings_headerLink_click(){
		Utilities.click(Sitewide_Elements.smSettings_headerLink_xpath());
		Utilities.assertEquals(Sitewide_Elements_Verify.isSMSettings_text_siteWide_verify(), true, "SMSettings link failed");
	}
	
	public static void userManagement_headerLink_click(){
		Utilities.click(Sitewide_Elements.userManagement_headerLink_xpath());
		Utilities.assertEquals(Sitewide_Elements_Verify.isUserManagement_text_siteWide_verify(), true, "User management link failed");
	}
	
	//////////////////Search Box/////////////////////////
	
	public static void searchBox_textBox_fill(String value){
		Utilities.sendKeys(Sitewide_Elements.searchBox_textBox_id(), value);
	}
	
	public static void search_button_click(){
		Utilities.click(Sitewide_Elements.search_button_id());
	}
	
	/*		Verifications		*/
	
}
