package SoluApp.SeleniumProofCase.pages_sitewide;

import java.lang.reflect.Method;

import org.openqa.selenium.By;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_candidateProfile.CandidateProfile_Actions;
import SoluApp.SeleniumProofCase.pages_candidateProfile.CandidateProfile_Elements_ViewMode;
import SoluApp.SeleniumProofCase.pages_candidateProfile.CandidateProfile_Elements_EditMode;

public class Sitewide_Elements_Verify {
	
	public static void verify() throws Exception{
		Method[] methods = Sitewide_Elements.class.getMethods();
		for (Method m : methods){
			if((m.getName().toLowerCase().contains("xpath") || m.getName().toLowerCase().contains("id")
					|| m.getName().toLowerCase().contains("linktext") || m.getName().toLowerCase().contains("classname")) &&
					(!m.getName().toLowerCase().contains("google") && !m.getName().toLowerCase().contains("dynamic")) ){
				System.out.println("Checking element: " + m.getName());
				Utilities.assertEquals(Utilities.doesElementExist((By)m.invoke(Sitewide_Elements.class)), true,
						"The element - " + m.getName() + " does not exist at - " + m.invoke(Sitewide_Elements.class));
			}
		}
	}
	
	public static boolean isDashboard_text_siteWide_verify(){
		if (Utilities.getText(Sitewide_Elements.pageTitle_text_xpath()).toLowerCase().equals("dashboard")){return true;}else{return false;}
	}
	
	public static boolean isCandidates_text_siteWide_verify(){
		if (Utilities.getText(Sitewide_Elements.pageTitle_text_xpath()).toLowerCase().equals("candidates")){return true;}else{return false;}
	}
	
	public static boolean isAssignTest_text_siteWide_verify(){
		if (Utilities.getText(Sitewide_Elements.pageTitle_text_xpath()).toLowerCase().equals("assign test")){return true;}else{return false;}
	}
	
	public static boolean isSGSettings_text_siteWide_verify(){
		if (Utilities.getText(Sitewide_Elements.pageTitle_text_xpath()).toLowerCase().equals("survey gizmo settings")){return true;}else{return false;}
	}
	
	public static boolean isSMSettings_text_siteWide_verify(){
		if (Utilities.getText(Sitewide_Elements.pageTitle_text_xpath()).toLowerCase().equals("staffing manager settings")){return true;}else{return false;}
	}
	
	public static boolean isUserManagement_text_siteWide_verify(){
		if (Utilities.getText(Sitewide_Elements.pageTitle_text_xpath()).toLowerCase().equals("user management")){return true;}else{return false;}
	}
	
	public static boolean isAssigned_text_siteWide_verify(){
		if (Utilities.getText(Sitewide_Elements.pageTitle_text_xpath()).toLowerCase().equals("assign test")){return true;}else{return false;}
	}
	
	public static boolean isCandidateProfile_text_siteWide_verify(){
		if (Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.heading_text_xpath())){
			if (Utilities.getText(CandidateProfile_Elements_ViewMode.heading_text_xpath()).equalsIgnoreCase("candidate profile")){return true;}
		}
		return false;
	}
	
	public static boolean isViewMode_buttonId_candidateProfile_verify(){
		if(isCandidateProfile_text_siteWide_verify()){
			System.out.println("Debug: is candidate profile page");
			if (Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.editMode_button_id())){
				return true;
			}
		}
		return false;
	}
	
	//OVERLOADED
	public static boolean isViewMode_buttonId_candidateProfile_verify(String fullName) throws Exception{
		if(isCandidateProfile_text_siteWide_verify()){
			System.out.println("Candidate profile verified");
			if (Utilities.doesElementExist(CandidateProfile_Elements_ViewMode.editMode_button_id())){
				System.out.println("View mode verified");
				CandidateProfile_Actions.verifyCandidate_viewMode(fullName);
				return true;
			}
		}
		return false;
	}
	
	public static boolean isEditMode_buttonId_candidateProfile_verify(){
		if(isCandidateProfile_text_siteWide_verify()){
			if (Utilities.doesElementExist(CandidateProfile_Elements_EditMode.viewMode_button_id())){
				return true;
			}
		}
		return false;
	}
	
	//OVERLOADED
	public static boolean isEditMode_buttonId_candidateProfile_verify(String fullName) throws Exception{
		if(isCandidateProfile_text_siteWide_verify()){
			if (Utilities.doesElementExist(CandidateProfile_Elements_EditMode.viewMode_button_id())){
				CandidateProfile_Actions.verifyCandidate_editMode(fullName);
				return true;
			}
		}
		return false;
	}
}
