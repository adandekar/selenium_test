package SoluApp.SeleniumProofCase;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;

import SoluApp.SeleniumProofCase.pages_candidates.*;
import SoluApp.SeleniumProofCase.pages_assignTest.*;
import SoluApp.SeleniumProofCase.pages_smSettings.*;
import SoluApp.SeleniumProofCase.pages_userManagement.*;
import SoluApp.SeleniumProofCase.pages_emailText.*;
import SoluApp.SeleniumProofCase.pages_sgSettings.SGSettings_Actions;
import SoluApp.SeleniumProofCase.pages_sgSettings.SGSettings_Elements_Actions;
import SoluApp.SeleniumProofCase.pages_sgSettings.SGSettings_Elements_Verify;
import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.pages_candidateProfile.*;
import SoluApp.SeleniumProofCase.pages_dashboard.Dashboard_Actions;
import SoluApp.SeleniumProofCase.pages_dashboard.Dashboard_Elements_Verify;
import SoluApp.SeleniumProofCase.pages_emailText.EmailText_Elements_Verify;

public class ProofCase{
	public static WebDriver driver;
	public int delay;
	private int customMaxWaitTime;
	
	@Parameters({"browserType", "delayTime", "customMaxWaitTime"})
	@BeforeClass
	public void setUp(String browser, int delayTime, int customMaxWaitTime) throws Exception {
		System.out.println("Started SetUp");
		if(browser.equalsIgnoreCase("firefox"))
		{
			driver = new FirefoxDriver();
		}
		else if(browser.equalsIgnoreCase("chrome"))
		{
			if(System.getProperty("os.name").startsWith("Windows")){
				System.setProperty("webdriver.chrome.driver", "chromedriver.exe");// windows code
			}
			else{
				System.setProperty("webdriver.chrome.driver", "chromedriver");// linux code
			}
			driver = new ChromeDriver();
		}
		System.out.println("Driver setup complete");
		delay = delayTime;
		this.customMaxWaitTime = customMaxWaitTime;
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
		System.out.println("Page timout setup complete");
		Utilities.setDriver(driver);
		Utilities.setWaitTime(customMaxWaitTime);
		Utilities.setDelay(delay); 
		Sitewide_Actions.resetPage();
		System.out.println("Finished SetUp");
	}

	@Test(priority = 1)
	public void signIn() throws Exception {
		System.out.println("Starting Signin");
		Sitewide_Elements_Actions.googleLogin_button_click();
		Sitewide_Actions.googleAuth("scerbone@solutechnology.com", "SMTDevelopment");
		System.out.println("Completed Signin");
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"})
	public void verifyElements() throws Exception{
		System.out.println("Running Verify Elements");
		Sitewide_Elements_Verify.verify();
		Dashboard_Elements_Verify.verify();
		Candidates_Elements_Verify.verify();
		AssignTest_Elements_Verify.verify();
		SGSettings_Elements_Verify.verify();
		EmailText_Elements_Verify.verify();
		SMSettings_Elements_Verify.verify();
		UserManagement_Elements_Verify.verify();
		CandidateProfile_Elements_Verify_ViewMode.verify();
		CandidateProfile_Elements_Verify_EditMode.verify();
	}
	
	///////////////////////////////Candidate Tests///////////////////////////////////////////
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateData")
	public void addCandidate_candidates(String firstName, String lastName, String email, String phoneNumber, String recruiter,
			String resourceType, String testType) throws Exception {
		System.out.println("Running addCandidate_candidates");
		boolean isValidInput = isValidInput_set(testType);
		boolean isUnique = isUnique_set(testType);
		
		Candidates_Actions.addCandidate(firstName, lastName, email, phoneNumber, recruiter, resourceType, isValidInput, isUnique);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateData")
	public void addCandidate_dashboard(String firstName, String lastName, String email, String phoneNumber, String recruiter,
			String resourceType, String testType) throws Exception {
		System.out.println("Running addCandidate_dashboard");
		boolean isValidInput = isValidInput_set(testType);
		boolean isUnique = isUnique_set(testType);
		
		Dashboard_Actions.addCandidate(firstName, lastName, email, phoneNumber, recruiter, resourceType, isValidInput, isUnique);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="editCanidateInformation")
	public void editCandidateInformation(String firstName, String lastName, String email, String phoneNumber, String address, String city, String state, String zipCode,
			String resourceType, String level, String location, String recruiter, String[] tags) throws Exception{
		System.out.println("Running editCandidateInformation");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Candidates_Actions.editCandidate(firstName + " " + lastName);
		Thread.sleep(delay);
		CandidateProfile_Actions.fillCandidateInformation(firstName, lastName, email, phoneNumber, address, city, state, zipCode, resourceType,
				level, location, recruiter, tags);
		CandidateProfile_Elements_Actions.save_button_click();
		CandidateProfile_Actions.verifyCandidate(firstName, lastName, email, phoneNumber, recruiter, resourceType, address, city, state, zipCode, level, location, tags);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateNames")
	public void viewCandidate_candidates(String candidateName) throws Exception{
		System.out.println("Running viewCandidate_candidates");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Candidates_Actions.viewCandidate(candidateName);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateNames")
	public void editCandidate_candidates(String candidateName) throws Exception{
		System.out.println("Running editCandidate_candidates");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Candidates_Actions.editCandidate(candidateName);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="searchData")
	public void viewCandidate_search(String candidateName, String searchTerm) throws Exception{
		System.out.println("Running viewCandidate_search");
		Sitewide_Elements_Actions.reset_headerLink_click();
		Sitewide_Actions.viewCandidate_search(candidateName, searchTerm);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="searchData")
	public void editCandidate_search(String candidateName, String searchTerm) throws Exception{
		System.out.println("Running editCandidate_search");
		Sitewide_Elements_Actions.reset_headerLink_click();
		Sitewide_Actions.editCandidate_search(candidateName, searchTerm);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateNames")
	public void removeCandidate_candidates(String candidateName) throws Exception{
		System.out.println("Running removeCandidate_candidates");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Candidates_Actions.removeCandidate(candidateName, customMaxWaitTime);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateNames")
	public void removeCandidate_search(String candidateName) throws Exception{
		System.out.println("Running removeCandidate_search");
		Sitewide_Elements_Actions.reset_headerLink_click();
		Sitewide_Actions.removeCandidate_search(candidateName, candidateName, customMaxWaitTime);
		Thread.sleep(delay);
	}
	
//	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateNames")
//	public void removeCandidateAgain(String candidateName, String searchTerm) throws Exception{
//		System.out.println("Running remove candidate again");
//		Sitewide_Elements_Actions.reset_headerLink_click();
//		Thread.sleep(delay);
//		Sitewide_Actions.removeCandidate_search(candidateName, searchTerm, customMaxWaitTime);
//		Thread.sleep(delay);
//		Candidates_Actions.removeCandidate(candidateName, customMaxWaitTime);
//		Thread.sleep(delay);
//	}
	
	/////////////////////////////Candidate Profile Information Tests//////////////////////////////////
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="profilePicData")
	public void addProfilePic(String candidateName, String fileName, String pictureSize) throws Exception{
		System.out.println("Running addProfilePic");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Candidates_Actions.editCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.addProfilePic(Utilities.resourceDirectory()+fileName, Integer.parseInt(pictureSize));
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="profilePicData")
	public void removeProfilePic(String candidateName, String fileName, String pictureSize) throws Exception{
		System.out.println("Running removeProfilePic");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Candidates_Actions.editCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.removeProfilePic(Integer.parseInt(pictureSize));
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="resumeData")
	public void addResume(String candidateName, String fileName) throws Exception{
		System.out.println("Running addResume");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Thread.sleep(delay);
		Candidates_Actions.editCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.addResume(Utilities.resourceDirectory()+fileName);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="resumeData")
	public void downloadResume(String candidateName, String fileName) throws Exception{
		System.out.println("Running downloadResume");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Thread.sleep(delay);
		Candidates_Actions.viewCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.downloadResume();
//		Thread.sleep(3000);
//		Utilities.driver.close();
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="socialMediaData")
	public void addSocialMedia(String candidateName, String heading, String link) throws Exception{
		System.out.println("Running addSocialMedia");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Thread.sleep(delay);
		Candidates_Actions.editCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.addSocialMedia(heading, link);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="socialMediaData")
	public void removeSocialMedia(String candidateName, String heading, String link) throws Exception{
		System.out.println("Running removeSocialMedia");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Thread.sleep(delay);
		Candidates_Actions.editCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.removeSocialMedia(heading, link);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="commentData")
	public void addComment(String candidateName, String comment) throws Exception{
		System.out.println("Running addComment");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Thread.sleep(delay);
		Candidates_Actions.viewCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.addComment(comment);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="editCommentData")
	public void editComment(String candidateName, String oldComment, String newComment) throws Exception{
		System.out.println("Running editComment");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Thread.sleep(delay);
		Candidates_Actions.viewCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.editComment(oldComment, newComment);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="editCommentData")
	public void removeComment(String candidateName, String oldComment, String newComment) throws Exception{
		System.out.println("Running removeComment");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Thread.sleep(delay);
		Candidates_Actions.viewCandidate(candidateName);
		Thread.sleep(delay);
		CandidateProfile_Actions.removeComment(newComment);
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="assessmentData_assignTest")
	public void assignAssessment_assignTestPage(String candidateName, String assessmentName) throws Exception{
		System.out.println("Runnning assignAssessment_assignTestPage");
		Sitewide_Elements_Actions.assignTest_headerLink_click();
		Thread.sleep(delay);
		AssignTest_Actions.sendAssessment(assessmentName, candidateName);
//		if (assessmentType.equals("SELF")){AssignTest_Actions.selfAssessment(candidateName);}
//		else if (assessmentType.equals("MATH_AND_LOGIC")){AssignTest_Actions.mathAndLogicAssessment(candidateName);}
//		else if (assessmentType.equals("TECHNICAL_KNOWLEDGE")){AssignTest_Actions.technicalKnowledgeAssessment(candidateName);}
		
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="assessmentData_candidateProfile")
	public void assignAssessment_candidateProfilePage(String candidateName, String[] assessmentList) throws Exception{
		System.out.println("Runnning assignAssessment_candidateProfilePage");
		System.out.println("Candidate name: " + candidateName);
		Candidates_Actions.viewCandidate(candidateName);
		Thread.sleep(delay);
		
		for(String myAssessment : assessmentList){
			CandidateProfile_Actions.selectAssessment(myAssessment);
		}
		
		Thread.sleep(delay);
		
		CandidateProfile_Actions.sendAssessments();
		
		Thread.sleep(delay);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateNames")
	public void sendThankYouEmail(String candidateName) throws Exception{
		System.out.println("Running SendThankYouEmail");
		CandidateProfile_Actions.sendThankYouEmail(candidateName);
	}
	
	//////////////////////////Mail Tests///////////////////////////////////
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="emailAssessmentData_assignTest")
	public void checkMail(String candidateName, String assessmentName) throws Exception{
		System.out.println("Running Check Mail");
		Sitewide_Elements_Actions.assignTest_headerLink_click();
		AssignTest_Actions.assignAssessment(assessmentName, candidateName);
		String subjectKeyword = CandidateProfile_Elements_Actions.getAssessmentSubject();
		String[] bodyKeywords = CandidateProfile_Elements_Actions.getAssessmentAdminBody();
		CandidateProfile_Elements_Actions.cancel_button_assignNewTest_click();
		GuerillaMail.load(CandidateProfile_Actions.getUserName(candidateName));
		GuerillaMail.checkForAssessment(subjectKeyword, bodyKeywords);
		Sitewide_Actions.resetPage();
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="assessmentData_candidateProfile")
	public void checkMail_multipleAssessments(String candidateName, String[] assessmentNames) throws Exception{
		System.out.println("Running checkMail_mulipleAssessments");
		Candidates_Actions.viewCandidate(candidateName);
		String userName = CandidateProfile_Elements_Actions.getUserName_text_get();
		System.out.println("UserName: " + userName);
		for(String assessmentName : assessmentNames){
			CandidateProfile_Actions.selectAssessment(assessmentName);
		}
		CandidateProfile_Elements_Actions.sendSelected_button_click();
		String subjectKeyword = CandidateProfile_Elements_Actions.getAssessmentSubject();
		String[] bodyKeywords = CandidateProfile_Elements_Actions.getAssessmentAdminBody();
		GuerillaMail.load(userName);
		GuerillaMail.checkForAssessment(subjectKeyword, bodyKeywords);
		Sitewide_Actions.resetPage();
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateNames")
	public void checkThankYouEmail(String candidateName) throws Exception{
		System.out.println("Running CheckThankYouEmail");
		GuerillaMail.load(CandidateProfile_Actions.getUserName(candidateName));
		GuerillaMail.checkForThankYou();
		Sitewide_Actions.resetPage();
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateNames")
	public void deleteMail(String candidateName) throws Exception{
		System.out.println("Running Delete Mail");
		GuerillaMail.load(CandidateProfile_Actions.getUserName(candidateName));
		GuerillaMail.deleteMail();
		Sitewide_Actions.resetPage();
	}
	
	///////////////////////////Other SMT Tests/////////////////////////////////
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="resourceTypeData")
	public void addResourceType(String resourceType) throws Exception{
		System.out.println("Running AddResourceType");
		Sitewide_Elements_Actions.smSettings_headerLink_click();
		SMSettings_Actions.addResourceType(resourceType);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="resourceTypeData")
	public void removeResourceType(String resourceType) throws Exception{
		System.out.println("Running RemoveResourceType");
		Sitewide_Elements_Actions.smSettings_headerLink_click();
		SMSettings_Actions.removeResourceType(resourceType);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateLevelData")
	public void addCandidateLevel(String candidateLevel) throws Exception{
		System.out.println("Running AddCandidateLevel");
		Sitewide_Elements_Actions.smSettings_headerLink_click();
		SMSettings_Actions.addCandidateLevel(candidateLevel);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="candidateLevelData")
	public void removeCandidateLevel(String candidateLevel) throws Exception{
		System.out.println("Running RemoveCandidateLevel");
		Sitewide_Elements_Actions.smSettings_headerLink_click();
		SMSettings_Actions.removeCandidateLevel(candidateLevel);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="profileStatusData")
	public void addProfileStatus(String profileStatusParent, String profileStatusName, String profileStatusColor) throws Exception{
		System.out.println("Running AddProfileStatus");
		Sitewide_Elements_Actions.smSettings_headerLink_click();
		SMSettings_Actions.addProfileStatus(profileStatusParent, profileStatusName, profileStatusColor);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="profileStatusData")
	public void removeProfileStatus(String profileStatusParent, String profileStatusName, String profileStatusColor) throws Exception{
		System.out.println("Running RemoveProfileStatus");
		Sitewide_Elements_Actions.smSettings_headerLink_click();
		SMSettings_Actions.removeProfileStatus(profileStatusParent, profileStatusName, profileStatusColor);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="userData")
	public void addUser(String firstName, String lastName, String emailAddress, String phoneNumber, String role, String otherNotes, String testType) throws Exception{
		System.out.println("Running addUser");
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		boolean isValidInput = isValidInput_set(testType);
		boolean isUnique = isUnique_set(testType);
		UserManagement_Actions.addUser(firstName, lastName, emailAddress, phoneNumber, role, otherNotes, isValidInput, isUnique);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="searchUserData")
	public void searchUser(String fullName, String emailAddress) throws Exception{
		System.out.println("Running searchUser_name");
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		String firstName = fullName.split(" ")[0];
		String lastName = fullName.split(" ")[1];
		String userName = emailAddress.split("@")[0];
		String domainName = emailAddress.split("@")[1];
		UserManagement_Actions.searchUser_name(firstName, fullName);
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		UserManagement_Actions.searchUser_name(lastName, fullName);
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		UserManagement_Actions.searchUser_name(fullName, fullName);
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		UserManagement_Actions.searchUser_email(userName, fullName);
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		UserManagement_Actions.searchUser_email(domainName, fullName);
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		UserManagement_Actions.searchUser_email(emailAddress, fullName);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="editUserData")
	public void editUserInformation(String firstName, String lastName, String phoneNumber, String role, String otherNotes) throws Exception{
		System.out.println("Running editUserInformation");
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		UserManagement_Actions.editUserInformation(firstName, lastName, phoneNumber, role, otherNotes);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="userNames")
	public void removeUser(String fullName) throws Exception{
		System.out.println("Running removeUser");
		Sitewide_Elements_Actions.userManagement_headerLink_click();
		UserManagement_Actions.removeUser(fullName, customMaxWaitTime);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="assessmentData")
	public void addAssessment(String assessmentName, String assessmentID, String quizActionID, String totalQue,
			String type) throws Exception{
		System.out.println("Running addAssessment");
		Sitewide_Elements_Actions.sgSettings_headerLink_click();
		SGSettings_Actions.addAssessment(assessmentName, assessmentID, quizActionID, totalQue, type);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="editAssessmentData")
	public void editAssessment(String assessmentName, String fromName, String fromEmail, String subject, String emailBody, String adminNotificationEmails,
			String adminNotificationSubject, String adminNotificationMessage) throws Exception{
		System.out.println("Running editAssessment");
		Sitewide_Elements_Actions.sgSettings_headerLink_click();
		SGSettings_Elements_Actions.emailText_link_click(assessmentName);
		EmailText_Actions.editEmailText(fromName, fromEmail, subject, emailBody, adminNotificationEmails, adminNotificationSubject, adminNotificationMessage);
	}
	
	@Test(dependsOnMethods={"signIn"}, dataProvider="assessmentData")
	public void removeAssessment(String assessmentName, String assessmentID, String quizActionID, String totalQue,
			String type) throws Exception{
		System.out.println("Running removeAssessment");
		Sitewide_Elements_Actions.sgSettings_headerLink_click();
		SGSettings_Actions.removeAssessment(assessmentName);
	}

	@AfterSuite
	public void tearDown() throws Exception {
		Thread.sleep(delay);
		driver.quit();
	}
	
	///////////////////*		DATA PROVIDERS		*/////////////////////////////
	
	@DataProvider(name="candidateData")
	public static Object[][] candidateData(){
		return new Object[][]{ {"John", "Doe", "johndoesolu@sharklasers.com", "5551112222", "", "", "VALID"},
								{"newStephen", "newCerbone", "newstephennewcerbonesolu@sharklasers.com", "1112223333", "Kristy Ninfo", "Software Engineer", "VALID"},
								{"", "lastname", "email@here.com", "phoneNumber", "", "", "INVALID_INPUT"},
								{"firstname", "", "email@here.com", "phoneNumber", "", "", "INVALID_INPUT"},
								{"firstname", "lastname", "", "phonenumber", "", "", "INVALID_INPUT"},
								{"firstname", "lastname", "email@here.com", "", "", "", "INVALID_INPUT"},
								{"Stephen", "Cerbone", "importsjc@gmail.com", "1112223333", "", "", "EXISTING_CANDIDATE"}
								/*{"firstname", "", "email@here.com", "phoneNumber", "", "", "INVALID_INPUT"} */};
	}
	
	@DataProvider(name="candidateNames")
	public static Object[][] candidateNames(){
		return new Object[][]{ {"John Doe"}, {"newStephen newCerbone"} };
	}
	
	@DataProvider(name="editCanidateInformation")
	public static Object[][] editCandidateInformation(){
		return new Object[][]{ {"John", "Doe", "johndoesolu@sharklasers.com", "1112223333",
			"123 SomeStreet", "SomeCity", "New York", "12345", "Software Engineer", "Junior", "SomeLocation", "Kristy Ninfo", new String[]{"sometag"} } };
	}
	
	@DataProvider(name="searchData")
	public static Object[][] searchData(){
		return new Object[][]{ {"John Doe", "John"}, {"John Doe", "Doe"}, {"newStephen newCerbone", "newStephen newCerbone"}, {"John Doe", "johndoesolu@sharklasers.com"},
								 {"John Doe", "johndoesolu"}, {"John Doe", "sharklasers.com"}, {"newStephen newCerbone", "1112223333"}, {"newStephen newCerbone", "111"},
								 {"newStephen newCerbone", "222"} };
	}
	
	@DataProvider(name="profilePicData")
	public static Object[][] profilePicData(){
		return new Object[][]{ {"newStephen newCerbone", "Profile_Pic.png", "51409"}, {"John Doe", "Profile_Pic.png", "51409"} };
	}
	
	@DataProvider(name="resumeData")
	public static Object[][] resumeData(){
		return new Object[][]{ {"newStephen newCerbone", "Resume.pdf"}, {"John Doe", "Resume.pdf"} };
	}
	
	@DataProvider(name="socialMediaData")
	public static Object[][] socialMediaData(){
		return new Object[][]{ {"newStephen newCerbone", "Facebook", "http://www.facebook.com"}, {"newStephen newCerbone", "Twitter", "http://www.twitter.com"} };
	}
	
	@DataProvider(name="commentData")
	public static Object[][] commentData(){
		return new Object[][]{ {"newStephen newCerbone", "This comment is only a test."}, {"newStephen newCerbone", "Another test...only a test."} };
	}
	
	@DataProvider(name="editCommentData")
	public static Object[][] editCommentData(){
		return new Object[][]{ {"newStephen newCerbone", "This comment is only a test.", "This comment has been changed!"},
				{"newStephen newCerbone", "Another test...only a test.", "Another comment edit successful..."} };
	}
	
	@DataProvider(name="assessmentData_assignTest")
	public static Object[][] assessmentData_assignTest(){
		return new Object[][]{ {"John Doe", "Self Assessment"}, {"John Doe", "Math and Logic Assessment"},
				{"newStephen newCerbone", "Self Assessment"}, {"newStephen newCerbone", "Technical Knowledge Assessment"},
				{"", "Self Assessment"}, {"", "Math and Logic Assessment"}, {"", "Technical Knowledge Assessment"} };
	}
	
	@DataProvider(name="assessmentData_candidateProfile")
	public static Object[][] assessmentData_candidateProfile(){
		return new Object[][]{ {"John Doe", new String[]{"Self Assessment"}}, {"John Doe", new String[]{"Math and Logic Assessment"}},
				{"newStephen newCerbone", new String[]{"Self Assessment", "Technical Knowledge Assessment"} } };
	}
	
	@DataProvider(name="emailAssessmentData_assignTest")
	public static Object[][] emailAssessmentData_assignTest(){
		return new Object[][]{ {"John Doe", "Self Assessment"}, {"John Doe", "Math and Logic Assessment"},
								{"newStephen newCerbone", "Self Assessment"}, {"newStephen newCerbone", "Technical Knowledge Assessment"} };
	}
	
	@DataProvider(name="emailAssessmentData_candidateProfile")
	public static Object[][] emailAssessmentData_candidateProfile(){
		return new Object[][]{ {"John Doe", "SELF"}, {"John Doe", "MATH_AND_LOGIC"}, {"newStephen newCerbone", "SELF", "TECHNICAL_KNOWLEDGE"} };
	}
	
	@DataProvider(name="resourceTypeData")
	public static Object[][] resourceTypeData(){
		return new Object[][]{ {"TestResource"}, {"FakeResource"} };
	}
	
	@DataProvider(name="candidateLevelData")
	public static Object[][] candidateLevelData(){
		return new Object[][]{ {"TestCandidateLevel"}, {"FakeCandidateLevel"} };
	}
	
	@DataProvider(name="assessmentData")
	public static Object[][] assessmentData(){
		return new Object[][]{ {"TestAssessment", "1111111", "111", "111", "Assessment"}, {"FakeAssessment", "2222222", "112", "112", "Assessment"} };
	}
	
	//String assessmentName, String fromName, String fromEmail, String subject, String emailBody, String adminNotificationEmails,
	//String adminNotificationSubject, String adminNotificationMessage
	@DataProvider(name="editAssessmentData")
	public static Object[][] editAssessmentData(){
		return new Object[][]{ {"TestAssessment", "[STP]", "solu-dev@solutechnology.com", "[STP] - Test Assessment",
			"Candidate Self-Assessment - A questionnaire whereby you rate your experience with a variety of skills and technologies.  "
			+ "You will be allowed to save your work and return to complete the self-assessment at any time.\n\nAssessment Link: [invite(\"self link\")]", 
			"mvernacotola@solutechnology.com,mchadbourne@solutechnology.com,jghidiu@solutechnology.com,jarndt@solutechnology.com", "", "[SOLU] Staffing Manager Admin, "
					+ "\n\nAn assessment was sent to [contact(\"first name\")] [contact(\"last name\")].\n\nCandidate Profile Link - [contact(\"profile link\")]"
					+ "\n\nYou will receive a confirmation email when this assessment is complete.\n\nThank You!\nThe Team @ [STP]"} };
	}
	
	@DataProvider(name="userData")
	public static Object[][] userData(){
		return new Object[][]{ {"John", "Doe", "someone@somewhere.com", "1112223333", "Administrator", "Test note", "VALID"},
								{"firstName", "lastName", "name@address.com", "1112223333", "Candidate", "", "VALID"},
								{"", "lastName", "name@address.com", "1112223333", "Candidate", "", "INVALID_INPUT"},
								{"firstName", "", "name@address.com", "1112223333", "Candidate", "", "INVALID_INPUT"},
								{"firstName", "lastName", "", "1112223333", "Candidate", "", "INVALID_INPUT"},
								{"firstName", "lastName", "name@address.com", "", "Candidate", "", "INVALID_INPUT"},
								{"Johny", "Doey", "someone@somewhere.com", "1112223333", "Administrator", "", "EXISTING_CANDIDATE"} };
	}
	
	@DataProvider(name="editUserData")
	public static Object[][] editUserData(){
		return new Object[][]{ {"John", "Doe", "1234567899", "Candidate", "Changed note"},
								{"firstName", "lastName", "9876543211", "Administrator", "This is the new note after the edit user information test"} };
	}
	
	@DataProvider(name="userNames")
	public static Object[][] userNames(){
		return new Object[][]{ {"John Doe"}, {"firstName lastName"} };
	}
	
	@DataProvider(name="searchUserData")
	public static Object[][] searchUserData(){
		return new Object[][]{ {"John Doe", "someone@somewhere.com"}, {"firstName lastName", "name@address.com"} };
	}
	
	@DataProvider(name="profileStatusData")
	public static Object[][] profileStatusData(){
		return new Object[][]{ {"HIRED", "TestStatus", ""}, {"RECRUITMENT ENDED", "OtherStatus", ""} };
	}
	
	//////////////////////////*		Utility Functions		*//////////////////////////
	
	public static boolean isValidInput_set(String testType){
		if (testType.equals("VALID")){
			return true;
		}
		else if (testType.equals("INVALID_INPUT")){
			return false;
		}
		else if (testType.equals("EXISTING_CANDIDATE")){
			return true;
		}
		return true;
	}
	
	public static boolean isUnique_set(String testType){
		if (testType.equals("VALID")){
			return true;
		}
		else if (testType.equals("INVALID_INPUT")){
			return true;
		}
		else if (testType.equals("EXISTING_CANDIDATE")){
			return false;
		}
		return true;
	}
}
