package SoluApp.SeleniumProofCase.pages_candidates;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_candidateProfile.CandidateProfile_Elements_ViewMode;

public class Candidates_Elements_Actions {
	private static WebElement element = null;
	
	public static void addNewCandidate_button_click(){
		element = Utilities.driver.findElement(Candidates_Elements.addNewCandidate_button_id());
		element.click();
		
		//Assert
		element = Utilities.driver.findElement(Candidates_Elements.candidatePopup_popup_id());
//		Utilities.assertEquals(element.getAttribute("style"), "display: block;", "Popup did not open properly");
		Utilities.assertEquals(element.isDisplayed(), true, "Popup failed to open");
	}
	
	///////////////////Popup////////////////////////////
	public static void firstName_textBox_createCandidateInstance_fill(String value){
		if(value != ""){
			element = Utilities.driver.findElement(Candidates_Elements.firstname_textBox_id());
			element.sendKeys(value);
		}
	}
	
	public static void lastName_textBox_createCandidateInstance_fill(String value){
		if(value != ""){
			element = Utilities.driver.findElement(Candidates_Elements.lastName_textBox_id());
			element.sendKeys(value);
		}
	}
	
	public static void email_textBox_createCandidateInstance_fill(String value){
		if(value != ""){
			element = Utilities.driver.findElement(Candidates_Elements.email_textBox_id());
			element.sendKeys(value);
		}
	}
	
	public static void phone_textBox_createCandidateInstance_fill(String value){
		if(value != ""){
			element = Utilities.driver.findElement(Candidates_Elements.phone_textBox_id());
			element.sendKeys(value);
		}
	}
	
	public static void recruiter_dropDown_createCandidateInstance_selectValue(String value){
		if(value != ""){
			element = Utilities.driver.findElement(Candidates_Elements.recruiter_dropDown_xpath());
			Select select = new Select(element);
			select.selectByVisibleText(value);
		}
	}
	
	public static void resourceType_dropDown_createCandidateInstance_selectValue(String value){
		if(value != ""){
			element = Utilities.driver.findElement(Candidates_Elements.resourceType_dropDown_xpath());
			Select select = new Select(element);
			select.selectByVisibleText(value);
		}
	}
	
	public static void save_button_createCandidateInstance_click(){
		element = Utilities.driver.findElement(Candidates_Elements.save_button_xpath());
		element.click();
	}
	
	public static void cancel_button_createCandidateInstance_click(){
		element = Utilities.driver.findElement(Candidates_Elements.cancel_button_xpath());
		element.click();
		
		//Assert
		element = Utilities.driver.findElement(Candidates_Elements.candidatePopup_popup_id());
//		Utilities.assertEquals(element.getAttribute("style"), "display: none;", "Popup did not close properly");
		Utilities.assertEquals(element.isDisplayed(), false, "Popup failed to open");
	}
	
	public static void proceedToProfile_button_createCandidateInstance_click(){
		element = Utilities.driver.findElement(Candidates_Elements.proceedToProfile_button_xpath());
		element.click();
		
		//TODO: test assert
		//Assert
		Utilities.assertEquals(Utilities.driver.findElement(CandidateProfile_Elements_ViewMode.heading_text_xpath()).getText().toLowerCase(), "new candidate profile", "Proceed to profile button failed");
	}
	
	////////////Page Buttons////////////
	public static void nextPage_button_click(){
		//TODO: add a page number verifier that makes sure it advances to the next page?
		element = Utilities.driver.findElement(Candidates_Elements.nextPage_dynamicButton_id());
		element.click();
	}
}
