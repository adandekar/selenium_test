package SoluApp.SeleniumProofCase.pages_candidates;

import org.openqa.selenium.By;

public class Candidates_Elements {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	public static By addNewCandidate_button_id(){
		return By.id("add_new_candidate");
	}
	
	public static By candidatePopup_popup_id(){
		return By.id("candidate_popup");
	}
	
	public static By firstname_textBox_id(){
		return By.id("first_name");
	}
	
	public static By lastName_textBox_id(){
		return By.id("last_name");
	}
	
	public static By email_textBox_id(){
		return By.id("email");
	}
	
	public static By phone_textBox_id(){
		return By.id("phone");
	}
	
	public static By recruiter_dropDown_xpath(){
		return By.xpath("//form[@id='candidate_popup_form']//select[@name='profile_details[recruiter]']");
	}
	
	public static By resourceType_dropDown_xpath(){
		return By.xpath("//form[@id='candidate_popup_form']//select[@name='profile_details[resource_type]']");
	}
	
	public static By save_button_xpath(){
		return By.xpath("//form[@id='candidate_popup_form']//input[@value='SAVE']");
	}
	
	public static By cancel_button_xpath(){
		return By.xpath("//form[@id='candidate_popup_form']//input[@value='CANCEL']");
	}
	
	public static By proceedToProfile_button_xpath(){
		return By.xpath("//form[@id='candidate_popup_form']//input[@value='PROCEED TO PROFILE']");
	}
	
	public static By nextPage_dynamicButton_id(){
		return By.id("page_next");
	}
}
