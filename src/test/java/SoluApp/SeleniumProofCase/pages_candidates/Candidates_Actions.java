package SoluApp.SeleniumProofCase.pages_candidates;

import java.util.List;

import org.openqa.selenium.WebElement;

import SoluApp.SeleniumProofCase.pages_candidateProfile.CandidateProfile_Actions;
import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.Utilities;

public class Candidates_Actions {
	
	public static void addCandidate(String firstName, String lastName, String email, String phoneNumber, String recruiter, String resourceType,
			boolean isValidInput, boolean isUnique) throws Exception {
		System.out.println("Running addCandidateFull");
		Sitewide_Elements_Actions.candidates_headerLink_click();
		Candidates_Elements_Actions.addNewCandidate_button_click();
		Sitewide_Actions.fillCandidateInformation(firstName, lastName, email, phoneNumber, recruiter, resourceType);
		Sitewide_Actions.save_createCandidateInstance(isValidInput, isUnique);
		
		if (isValidInput && isUnique){CandidateProfile_Actions.verifyCandidate(firstName, lastName, email, phoneNumber, recruiter, resourceType);}
		Thread.sleep(Utilities.delay);
	}
	
	/**
	 * Find and return a candidate from the candidates page
	 * @param driver
	 * @param candidateName
	 * @return
	 */
	public static WebElement findCandidate_candidates(String candidateName) {
		System.out.println("Finding canidate: " + candidateName);
		while (true) {
			List<WebElement> elements = Utilities.driver.findElements(Sitewide_Elements.getList_dynamicList_xpath());
			for (WebElement myElement : elements) {
				String value = Utilities.extractCandidateName(myElement.findElement(Sitewide_Elements.detail1_dynamicList_classname()).getText());
				if (candidateName.equalsIgnoreCase(value)) {
					return myElement;
				}
			}
			if (Utilities.doesElementExist(Candidates_Elements.nextPage_dynamicButton_id())) {
				Candidates_Elements_Actions.nextPage_button_click();
			} else {
				break;
			}
		}
		return null;
	}
	
	/**
	 * Find candidate and click the view link.
	 * @Verification check if the candidate exists and if the view link brings up the correct page
	 * @param driver
	 * @param candidateName
	 * @throws Exception
	 */
	public static void viewCandidate(String candidateName) throws Exception {
		Sitewide_Elements_Actions.candidates_headerLink_click();
		WebElement myElement = findCandidate_candidates(candidateName);
		Utilities.assertNotEquals(myElement, null, "Candidate does not exist");
		Utilities.click(myElement.findElement(Sitewide_Elements.viewListElement_dynamicList_linkText()));
		Utilities.assertEquals(Sitewide_Elements_Verify.isViewMode_buttonId_candidateProfile_verify(candidateName), true, "View link failed");
	}
	
	/**
	 * Find a candidate and click the edit link
	 * @Verification check if the candidate exists and if the edit link brings up the correct page
	 * @param driver
	 * @param candidateName
	 * @throws Exception
	 */
	public static void editCandidate(String candidateName) throws Exception {
		WebElement myElement = findCandidate_candidates(candidateName);
		Utilities.assertNotEquals(myElement, null, "Candidate: " + candidateName + " does not exist");
		Utilities.click(findCandidate_candidates(candidateName).findElement(Sitewide_Elements.editListElement_dynamicList_linkText()));
//		myElement.findElement(Sitewide_Elements.editListElement_dynamicList_linkText()).click();
		Utilities.assertEquals(Sitewide_Elements_Verify.isEditMode_buttonId_candidateProfile_verify(candidateName), true, "Edit link failed");
	}
	
	/**
	 * Find a candidate and remove it
	 * @Verification check if the candidate exists, if the candidate is remove message is displayed, and if the candidate exists after the removal
	 * @param driver
	 * @param candidateName
	 * @param customMaxWaitTime
	 * @throws Exception
	 */
	public static void removeCandidate(String candidateName, int customMaxWaitTime) throws Exception {
		WebElement myElement = findCandidate_candidates(candidateName);

		Utilities.assertNotEquals(myElement, null, "Candidate does not exist");
		myElement.findElement(Sitewide_Elements.removeListElement_dynamicList_linkText()).click();
		Utilities.acceptAlert(customMaxWaitTime);
		Utilities.acceptAlert(customMaxWaitTime);
		Utilities.assertEquals(Utilities.doesElementExist(Sitewide_Elements.errorMessage_dynamicText_xpath()), true, "Candidate removed message is missing");
		Utilities.assertEquals(findCandidate_candidates(candidateName), null, "Candidate was not properly removed");
	}
}
