package SoluApp.SeleniumProofCase.pages_sgSettings;

import org.openqa.selenium.By;

public class SGSettings_Elements {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	/////////////////Assessment List//////////////////////
	
	public static By assessmentList_list_xpath(){
		return By.xpath("(//div[@id='details_new_assessment'])[3]//div[contains(@id,'data_')]");
	}
	
	public static By emailText_dynamicLink_linkText(){
		return By.linkText("EMAIL TEXT");
	}
	
	public static By resultTextFile_dynamicLink_linkText(){
		return By.linkText("RESULT TEXT FILE");
	}
	
	////////////////Assessment Data Input/////////////////
	
	public static By assessmentName_textBox_id(){
		return By.id("assessment_name");
	}
	
	public static By assessmentID_textBox_id(){
		return By.id("assessment_id");
	}
	
	public static By quizActionID_textBox_id(){
		return By.id("quiz_action_id");
	}
	
	public static By totalQue_textBox_id(){
		return By.id("total_questions");
	}
	
	public static By type_dropDown_id(){
		return By.id("survey_type");
	}
	
	public static By addAssessment_button_id(){
		return By.id("add_new_assessment_btn");
	}
	
}
