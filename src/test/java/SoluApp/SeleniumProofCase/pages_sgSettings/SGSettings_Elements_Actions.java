package SoluApp.SeleniumProofCase.pages_sgSettings;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_sitewide.Sitewide_Elements;

public class SGSettings_Elements_Actions {
	private static WebElement element = null;
	
	public static void assessmentName_textBox_fill(String value){
		element = Utilities.driver.findElement(SGSettings_Elements.assessmentName_textBox_id());
		element.sendKeys(value);
	}
	
	public static void assessmentID_textBox_fill(String value){
		element = Utilities.driver.findElement(SGSettings_Elements.assessmentID_textBox_id());
		element.sendKeys(value);
	}
	
	public static void quizActionID_textBox_fill(String value){
		element = Utilities.driver.findElement(SGSettings_Elements.quizActionID_textBox_id());
		element.sendKeys(value);
	}
	
	public static void totalQue_textBox_fill(String value){
		element = Utilities.driver.findElement(SGSettings_Elements.totalQue_textBox_id());
		element.sendKeys(value);
	}
	
	public static void type_dropDown_select(String value){
		if(value != ""){
			element = Utilities.driver.findElement(SGSettings_Elements.type_dropDown_id());
			Select select = new Select(element);
			select.selectByVisibleText(value);
		}
	}
	
	public static void add_button_click(){
		element = Utilities.driver.findElement(SGSettings_Elements.addAssessment_button_id());
		element.click();
	}
	
	//////////////////Assessment list//////////////////
	public static WebElement findAssessment(String assessmentName){
		List<WebElement> elementList = Utilities.driver.findElements(SGSettings_Elements.assessmentList_list_xpath());
		
		for(WebElement myElement : elementList){
			if(myElement.findElement(By.className("detail_1")).getText().equalsIgnoreCase(assessmentName)){
				return myElement;
			}
		}
		
		return null;
	}
	
	public static void emailText_link_click(String assessmentName){
		element = findAssessment(assessmentName);
		element.findElement(SGSettings_Elements.emailText_dynamicLink_linkText()).click();
	}
	
	public static void remove_link_click(String assessmentName){
		element = findAssessment(assessmentName);
		element.findElement(Sitewide_Elements.removeListElement_dynamicList_linkText()).click();
	}
}
