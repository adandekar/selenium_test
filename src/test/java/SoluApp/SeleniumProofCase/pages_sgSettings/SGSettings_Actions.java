package SoluApp.SeleniumProofCase.pages_sgSettings;

import SoluApp.SeleniumProofCase.Utilities;
import SoluApp.SeleniumProofCase.pages_assignTest.AssignTest_Elements_Actions;
import SoluApp.SeleniumProofCase.pages_sitewide.Sitewide_Elements_Actions;

public class SGSettings_Actions {
	/**
	 * Add assessment in the SG Settings page
	 * @param assessmentName
	 * @param sgID
	 * @param quizActionID
	 * @param totalQue
	 * @param type
	 */
	public static void addAssessment(String assessmentName, String sgID, String quizActionID, String totalQue, String type){
		SGSettings_Elements_Actions.assessmentName_textBox_fill(assessmentName);
		SGSettings_Elements_Actions.assessmentID_textBox_fill(sgID);
		SGSettings_Elements_Actions.quizActionID_textBox_fill(quizActionID);
		SGSettings_Elements_Actions.totalQue_textBox_fill(totalQue);
		SGSettings_Elements_Actions.type_dropDown_select(type);
		SGSettings_Elements_Actions.add_button_click();
		Utilities.assertNotEquals(SGSettings_Elements_Actions.findAssessment(assessmentName), null, "Assessment not properly added, not in the SG Settings list");
		Sitewide_Elements_Actions.assignTest_headerLink_click();
		Utilities.assertNotEquals(AssignTest_Elements_Actions.findAssessment(assessmentName), null, "Assessment not properly added, not in the Assign Test list");
	}
	
	public static void removeAssessment(String assessmentName) throws Exception{
		SGSettings_Elements_Actions.remove_link_click(assessmentName);
		Utilities.acceptAlert(Utilities.customMaxWaitTime);
		Utilities.assertEquals(SGSettings_Elements_Actions.findAssessment(assessmentName), null, "Assessment not properly removed, SG Settings list.");
		Sitewide_Elements_Actions.assignTest_headerLink_click();
		Utilities.assertEquals(AssignTest_Elements_Actions.findAssessment(assessmentName), null, "Assessment not properly removed, Assign Test list.");
	}
}
