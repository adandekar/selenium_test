package SoluApp.SeleniumProofCase.pages_userManagement;

import org.openqa.selenium.By;

public class UserManagement_Elements {
	//naming convention: elementName_elementType_elementReferenceType
	//naming example: googleLogin_button_xpath
	
	public static By heading_text_xpath(){
		return By.xpath("//div[@id='profile_div']/h1[@class='admin_profile_heading']");
	}
	
	/////////////User Information//////////////////
	
	public static By addNewUser_button_xpath(){
		return By.id("add_new_user_button");
	}
	
	public static By firstName_textBox_id(){
		return By.id("first_name_admin");
	}
	
	public static By lastName_textBox_id(){
		return By.id("last_name_admin");
	}
	
	public static By emailAddress_textBox_id(){
		return By.id("email_admin");
	}
	
	public static By phoneNumber_textBox_id(){
		return By.id("phone_admin");
	}
	
	public static By role_dropDown_id(){
		return By.id("role_admin");
	}
	
	public static By otherNotes_textArea_xpath(){
		return By.xpath("//form[@id='admin_profile']//textarea[contains(@name,'profile_details')]");
	}
	
	public static By save_button_xpath(){
		return By.xpath("//form[@id='admin_profile']//input[@class='button_style' and @value='SAVE']");
	}
	
	public static By cancel_button_xpath(){
		return By.xpath("//form[@id='admin_profile']//input[@class='button_style' and @value='CANCEL']");
	}
	
	///////////////////User List//////////////////
	
	public static By nameSearch_textBox_id(){
		return By.id("search_name");
	}
	
	public static By nameGo_button_id(){
		return By.id("search_name_go");
	}
	
	public static By emailSearch_textBox_id(){
		return By.id("search_email");
	}
	
	public static By emailGo_button_id(){
		return By.id("search_email_go");
	}
	
	public static By sortByName_link_id(){
		return By.id("sort_by_name");
	}
	
	public static By sortByRole_link_id(){
		return By.id("sort_by_role");
	}
	
	public static By sortByEmail_link_id(){
		return By.id("sort_by_email");
	}
	
	public static By userList_list_xpath(){
		return By.xpath("//div[@id='admin_users']//div[contains(@id,'data_')]");
	}
}
