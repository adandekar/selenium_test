package SoluApp.SeleniumProofCase.pages_userManagement;

import SoluApp.SeleniumProofCase.Utilities;

public class UserManagement_Elements_Actions {
	
	public static void addNewUser_button_click(){
		Utilities.click(UserManagement_Elements.addNewUser_button_xpath());
		Utilities.assertEquals(Utilities.driver.findElement(UserManagement_Elements.heading_text_xpath()).getText().toLowerCase(), "add new user", "User expansion failed to open");
	}
	
	public static void firstName_textBox_fill(String value){
		Utilities.sendKeys(UserManagement_Elements.firstName_textBox_id(), value);
	}
	
	public static void lastName_textBox_fill(String value){
		Utilities.sendKeys(UserManagement_Elements.lastName_textBox_id(), value);
	}
	
	public static void emailAddress_textBox_fill(String value){
		Utilities.sendKeys(UserManagement_Elements.emailAddress_textBox_id(), value);
	}
	
	public static void phoneNumber_textBox_fill(String value){
		Utilities.sendKeys(UserManagement_Elements.phoneNumber_textBox_id(), value);
	}
	
	public static void role_dropDown_selectValue(String value){
		Utilities.selectByVisibleText(UserManagement_Elements.role_dropDown_id(), value);
	}
	
	public static void otherNotes_textArea_fill(String value){
		Utilities.sendKeys(UserManagement_Elements.otherNotes_textArea_xpath(), value);
	}
	
	public static void save_button_click(){
		Utilities.click(UserManagement_Elements.save_button_xpath());
	}
	
	public static void cancel_button_click(){
		Utilities.click(UserManagement_Elements.cancel_button_xpath());
	}
	
	/////////////////Search Elements//////////////////
	
	public static void nameSearch_textBox_fill(String value){
		Utilities.sendKeys(UserManagement_Elements.nameSearch_textBox_id(), value);
	}
	
	public static void nameSearchGo_button_click(){
		Utilities.click(UserManagement_Elements.nameGo_button_id());
	}
	
	public static void emailSearch_textBox_fill(String value){
		Utilities.sendKeys(UserManagement_Elements.emailSearch_textBox_id(), value);
	}
	
	public static void emailSearchGo_button_click(){
		Utilities.click(UserManagement_Elements.emailGo_button_id());
	}
}
