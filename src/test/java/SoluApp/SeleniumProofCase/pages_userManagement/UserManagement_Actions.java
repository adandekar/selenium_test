package SoluApp.SeleniumProofCase.pages_userManagement;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import SoluApp.SeleniumProofCase.pages_sitewide.*;
import SoluApp.SeleniumProofCase.Utilities;

public class UserManagement_Actions {
	
	public static void fillUserInformation(String firstName, String lastName, String emailAddress, String phoneNumber, String role, String otherNotes){
		UserManagement_Elements_Actions.firstName_textBox_fill(firstName);
		UserManagement_Elements_Actions.lastName_textBox_fill(lastName);
		UserManagement_Elements_Actions.emailAddress_textBox_fill(emailAddress);
		UserManagement_Elements_Actions.phoneNumber_textBox_fill(phoneNumber);
		UserManagement_Elements_Actions.role_dropDown_selectValue(role);
		UserManagement_Elements_Actions.otherNotes_textArea_fill(otherNotes);
		UserManagement_Elements_Actions.save_button_click();
	}
	
	public static void fillUserInformation(String firstName, String lastName, String phoneNumber, String role, String otherNotes){
		UserManagement_Elements_Actions.firstName_textBox_fill(firstName);
		UserManagement_Elements_Actions.lastName_textBox_fill(lastName);
		UserManagement_Elements_Actions.phoneNumber_textBox_fill(phoneNumber);
		UserManagement_Elements_Actions.role_dropDown_selectValue(role);
		UserManagement_Elements_Actions.otherNotes_textArea_fill(otherNotes);
		UserManagement_Elements_Actions.save_button_click();
		Utilities.assertEquals(UserManagement_Actions.checkUser(firstName, lastName, phoneNumber, role, otherNotes), true, "Check User Failed");
	}
	
	public static void clearUserInformation(){
		Utilities.clearElement(UserManagement_Elements.firstName_textBox_id());
		Utilities.clearElement(UserManagement_Elements.lastName_textBox_id());
		Utilities.clearElement(UserManagement_Elements.phoneNumber_textBox_id());
		Utilities.clearElement(UserManagement_Elements.otherNotes_textArea_xpath());
	}
	
	/**
	 * Add a user.
	 * @Verification check if the user expansion closed, there was no error message, and that the user exists.
	 * @param driver
	 * @param firstName
	 * @param lastName
	 * @param emailAddress
	 * @param phoneNumber
	 * @param role
	 * @param otherNotes
	 * @param isValidInput
	 * @param isUnique
	 * @throws Exception
	 */
	public static void addUser(String firstName, String lastName, String emailAddress, String phoneNumber, String role,
			String otherNotes, boolean isValidInput, boolean isUnique) throws Exception {
		UserManagement_Elements_Actions.addNewUser_button_click();
		fillUserInformation(firstName, lastName, emailAddress, phoneNumber, role, otherNotes);
		// TODO: check if the add new user expansion is open to test for valid and unique input
		Utilities.assertEquals(isExpansionOpen(), isValidInput, "Not Valid User Input");
		Utilities.assertEquals(!Utilities.doesElementExist(By.xpath("//div[@id='profile_div']//span[@class='error_message' and @style]")), isUnique,
				"Candiate Already Exists");

		if (isUnique && isValidInput) {
			System.out.println("Adding User");
			Utilities.assertEquals(UserManagement_Actions.checkUser(firstName, lastName, emailAddress, phoneNumber, role, otherNotes), true,
					"Check User Failed");
		}
	}
	
	/**
	 * Find a user by full name and return it.
	 * @param driver
	 * @param fullName
	 * @return
	 */
	public static WebElement findUser(String fullName) {
//		element = Utilities.driver.findElement(UserManagement_Elements.userList_list_id());
		List<WebElement> elementList = Utilities.driver.findElements(UserManagement_Elements.userList_list_xpath());
		for (WebElement myElement : elementList) {
			if (fullName.equalsIgnoreCase(Utilities.getText(myElement.findElement(By.xpath(".//div[@class='detail_1']"))))) {
				return myElement;
			}
		}

		return null;
	}
	
	/**
	 * Find a user in the list and click edit.
	 * @param driver
	 * @param fullName
	 */
	public static void editUser(String fullName) {
		WebElement myElement = UserManagement_Actions.findUser(fullName);
		Utilities.assertNotEquals(myElement, null, fullName + "not found in the list");
		myElement.findElement(Sitewide_Elements.editListElement_dynamicList_linkText()).click();
	}
	
	/**
	 * Find and edit a user's information.
	 * @param driver
	 * @param fullName
	 */
	public static void editUserInformation(String firstName, String lastName, String phoneNumber, String role, String otherNotes) {
		editUser(firstName + " " + lastName);
		clearUserInformation();
		fillUserInformation(firstName, lastName, phoneNumber, role, otherNotes);
	}
	
	/**
	 * Find and remove a user.
	 * @Verification check if the user still exists
	 * @param driver
	 * @param fullName
	 * @param customMaxWaitTime
	 * @throws Exception
	 */
	public static void removeUser(String fullName, int customMaxWaitTime) throws Exception {
		WebElement myElement = UserManagement_Actions.findUser(fullName);
		myElement.findElement(Sitewide_Elements.removeListElement_dynamicList_linkText()).click();
		Utilities.acceptAlert(customMaxWaitTime);
		Utilities.assertEquals(UserManagement_Actions.findUser(fullName), null, "User not removed properly");
	}
	
	/**
	 * Check if user information matches the information provided.
	 * @param driver
	 * @param firstName
	 * @param lastName
	 * @param emailAddress
	 * @param phoneNumber
	 * @param role
	 * @param otherNotes
	 * @return
	 */
	public static boolean checkUser(String firstName, String lastName, String emailAddress, String phoneNumber, String role,
			String otherNotes) {
		if (Sitewide_Elements_Verify.isUserManagement_text_siteWide_verify()) {
			UserManagement_Actions.editUser(firstName + " " + lastName);
			if (firstName.equalsIgnoreCase(Utilities.getAttribute(UserManagement_Elements.firstName_textBox_id(),"value"))
					&& lastName.equalsIgnoreCase(Utilities.getAttribute(UserManagement_Elements.lastName_textBox_id(),"value"))
					&& emailAddress.equalsIgnoreCase(Utilities.getAttribute(UserManagement_Elements.emailAddress_textBox_id(),"value"))
					&& phoneNumber.equalsIgnoreCase(Utilities.getAttribute(UserManagement_Elements.phoneNumber_textBox_id(),"value"))
					&& role.equalsIgnoreCase(Utilities.getSelectedValue(Utilities.driver.findElement(UserManagement_Elements.role_dropDown_id())))) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean checkUser(String firstName, String lastName, String phoneNumber, String role,
			String otherNotes) {
		if (Sitewide_Elements_Verify.isUserManagement_text_siteWide_verify()) {
			UserManagement_Actions.editUser(firstName + " " + lastName);
			if (firstName.equalsIgnoreCase(Utilities.getAttribute(UserManagement_Elements.firstName_textBox_id(),"value"))
					&& lastName.equalsIgnoreCase(Utilities.getAttribute(UserManagement_Elements.lastName_textBox_id(),"value"))
					&& phoneNumber.equalsIgnoreCase(Utilities.getAttribute(UserManagement_Elements.phoneNumber_textBox_id(),"value"))
					&& role.equalsIgnoreCase(Utilities.getSelectedValue(Utilities.driver.findElement(UserManagement_Elements.role_dropDown_id())))) {
				return true;
			}
		}
		return false;
	}
	
	/*		Search Function		*/
	
	public static void searchUser_name(String searchTerm, String fullName){
		UserManagement_Elements_Actions.nameSearch_textBox_fill(searchTerm);
		UserManagement_Elements_Actions.nameSearchGo_button_click();
		Utilities.assertNotEquals(UserManagement_Actions.findUser(fullName), null, "User did not show up in the list, search by name");
	}
	
	public static void searchUser_email(String searchTerm, String fullName){
		UserManagement_Elements_Actions.emailSearch_textBox_fill(searchTerm);
		UserManagement_Elements_Actions.emailSearchGo_button_click();
		Utilities.assertNotEquals(UserManagement_Actions.findUser(fullName), null, "User did not show up in the list, search by email");
	}
	
	/*		Utility Functions		*/
	/**
	 * Check if the user expansion is open or closed and return its state (T/F)
	 * @param driver
	 * @return False: Expansion is open. True: Expansion is closed.
	 */
	public static boolean isExpansionOpen(){
		if (Utilities.getAttribute(By.id("profile_div"), "style").equals("display: inline;")){return false;}
		return true;
	}
	
}
